////////////////////////////////////////////////////////////////////////////
// DotSceneOctreeManager.h
// Author     : Doug Wolanick
// Start Date : May 27, 2003
// Copyright  : (C) 2003 by Doug Wolanick
// Email      : Doug@IceTecStudios.com
////////////////////////////////////////////////////////////////////////////

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU Lesser General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#ifndef _DOT_SCENE_MANAGER_H
#define _DOT_SCENE_MANAGER_H

#include "OgreSceneManager.h"
#include "SceneMain.h"
#include "SceneOctreeRenderable.h"
#include "tinyxml.h"
#include <vector>

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32 && !defined ( __MINGW32__ )
#	if defined( PLUGIN_DOTSCENEMANAGER_EXPORTS )
#		define _PluginDotSceneManagerExport __declspec( dllexport )
#	else
#		define _PluginDotSceneManagerExport __declspec( dllimport )
#	endif
#else
#	define _PluginDotSceneManagerExport
#endif 

namespace Ogre
{
	class _PluginDotSceneManagerExport DotSceneOctreeManager : public SceneManager
	{
		friend class DotSceneOctreeIntersectionSceneQuery;
	public:
		DotSceneOctreeManager(const String& name);
		virtual ~DotSceneOctreeManager( );

		void setWorldGeometry( const String& filename );

		// Loads the XML and Binary data files to Memory //
		virtual void addWorldGeometry( const String& filename, const String& groupname );
		virtual void addWorldGeometry( const String& filename , const String& name, float xOffset, float zOffset, const String& groupname );

		void removeWorldGeometry( const String& name );
		void removeAllWorldGeometry( void );

		void setOctreeVisible( bool Visible );

		bool hasWorldGeometry() {
			if (!mSceneMain) return false;
			return (mSceneMain->getNumMeshes(NULL)>0);
		}

		// Return the Geometry Data For Collision //
		int   getNumMeshes( char* name ){
			return mSceneMain->getNumMeshes( name );
		}

		float* getVerticesPtr( char* name, int MeshIndex  ){ 
			return mSceneMain->getVerticesPtr( name, MeshIndex );
		}

		int*  getIndexPtr( char* name, int MeshIndex ){
			return mSceneMain->getIndexPtr( name, MeshIndex );
		}

		int getNumVertices( char* name, int MeshIndex  ){ 
			return mSceneMain->getNumVertices( name, MeshIndex );
		}

		int getNumIndex( char* name, int MeshIndex ){
			return mSceneMain->getNumIndex( name, MeshIndex );
		}

		const AxisAlignedBox& getBoundingBox( char* name, int MeshIndex )
		{
			return mSceneMain->getBoundingBox( name, MeshIndex );
		};

		// Currently Does nothing. Can be used to Dynamicaly load and unload data. //
		virtual void _updateSceneGraph( Camera * cam );
		// Renders Visible Objects //
		virtual void _renderVisibleObjects( void );
		// Nothing Special Here //
		//virtual void _findVisibleObjects ( Camera * cam, VisibleObjectsBoundsInfo* visibleBounds, bool onlyShadowCasters );

		bool setOption( const String& strKey, const void* pValue ); 
		bool getOption( const String& strKey, void* pDestValue ); 
		bool hasOption( const String& strKey ) const; 
		bool getOptionValues( const String& strKey, StringVector& refValueList );
		bool getOptionKeys( StringVector& refKeys );

		virtual void clearScene( void );

		virtual IntersectionSceneQuery* createIntersectionQuery(unsigned long mask = 0xFFFFFFFF);

		const String& getTypeName(void) const;

	protected:

		bool OctreeVisible;
		DotSceneMain	*mSceneMain;
		std::vector <DotSceneMain*> LoadedScenes;
	};

	/// Factory for OctreeSceneManager
	class DotSceneOctreeManagerFactory : public SceneManagerFactory
	{
	protected:
		void initMetaData(void) const;
	public:
		DotSceneOctreeManagerFactory() {}
		~DotSceneOctreeManagerFactory() {}
		/// Factory type name
		static const String FACTORY_TYPE_NAME;
		SceneManager* createInstance(const String& instanceName);
		void destroyInstance(SceneManager* instance);
	};

	/** DotScene specialisation of IntersectionSceneQuery */
	class DotSceneOctreeIntersectionSceneQuery : public DefaultIntersectionSceneQuery
	{
	public:
		DotSceneOctreeIntersectionSceneQuery(SceneManager* creator);

		/** See IntersectionSceneQuery. */
		void execute(IntersectionSceneQueryListener* listener);

	};

} //End namespace
#endif
