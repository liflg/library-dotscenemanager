# Microsoft Developer Studio Project File - Name="Plugin_DotSceneManager" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=Plugin_DotSceneManager - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Plugin_DotSceneManager.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Plugin_DotSceneManager.mak" CFG="Plugin_DotSceneManager - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Plugin_DotSceneManager - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "Plugin_DotSceneManager - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Plugin_DotSceneManager - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "..\bin\$(ConfigurationName)"
# PROP BASE Intermediate_Dir "..\obj\$(ConfigurationName)"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\bin\debug"
# PROP Intermediate_Dir "..\obj\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "../include" /I "../../../../../ogrenew/OgreMain/include" /I "..\..\..\..\..\ogrenew\Dependencies\include" /I "..\..\..\..\..\ogrenew\Tools\XMLConverter\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_USRDLL" /D "PLUGIN_DOTSCENEMANAGER_EXPORTS" /D "_MBCS" /FR /Fo".\..\obj\Debug/" /Fd".\..\obj\Debug/" /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "../include" /I "../../../../../ogrenew/OgreMain/include" /I "..\..\..\..\..\ogrenew\Dependencies\include" /I "..\..\..\..\..\ogrenew\Tools\XMLConverter\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_USRDLL" /D "PLUGIN_DOTSCENEMANAGER_EXPORTS" /D "_MBCS" /FR /Fo".\..\obj\Debug/" /Fd".\..\obj\Debug/" /GZ /c
# ADD BASE MTL /nologo /win32
# ADD MTL /nologo /win32
# ADD BASE RSC /l 0x809 /d "_DEBUG"
# ADD RSC /l 0x809 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ogremain.lib /nologo /subsystem:console /dll /pdb:".\..\bin\Debug\LWO2MESH.pdb" /debug /machine:I386 /def:"..\misc\DotSceneManager.def" /pdbtype:sept /libpath:"../../../../../ogrenew\Dependencies\lib\$(ConfigurationName)" /libpath:"../../../../../ogrenew\OgreMain\lib\$(ConfigurationName)"
# SUBTRACT BASE LINK32 /pdb:none
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ogremain.lib /nologo /subsystem:console /dll /pdb:".\..\bin\Debug\LWO2MESH.pdb" /debug /machine:I386 /def:"..\misc\DotSceneManager.def" /pdbtype:sept /libpath:"../../../../../ogrenew\Dependencies\lib\debug" /libpath:"../../../../../ogrenew\OgreMain\lib\debug"
# SUBTRACT LINK32 /pdb:none
# Begin Special Build Tool
OutDir=.\..\bin\debug
SOURCE="$(InputPath)"
PostBuild_Cmds=copy $(OutDir)\$(TargetFileName) ..\..\..\..\..\ogrenew\Samples\Common\bin\debug
# End Special Build Tool

!ELSEIF  "$(CFG)" == "Plugin_DotSceneManager - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "..\bin\$(ConfigurationName)"
# PROP BASE Intermediate_Dir "..\obj\$(ConfigurationName)"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\bin\release"
# PROP Intermediate_Dir "..\obj\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /Ob2 /Gy /I "../include" /I "../../../../../ogrenew/OgreMain/include" /I "..\..\..\..\..\ogrenew\Dependencies\include" /I "..\..\..\..\..\ogrenew\Tools\XMLConverter\include" /D "WIN32" /D "_WINDOWS" /D "_USRDLL" /D "PLUGIN_DOTSCENEMANAGER_EXPORTS" /D "_MBCS" /FR /Fo".\..\obj\Release/" /GF /c
# ADD CPP /nologo /MD /W3 /GX /Ob2 /Gy /I "../include" /I "../../../../../ogrenew/OgreMain/include" /I "..\..\..\..\..\ogrenew\Dependencies\include" /I "..\..\..\..\..\ogrenew\Tools\XMLConverter\include" /D "WIN32" /D "_WINDOWS" /D "_USRDLL" /D "PLUGIN_DOTSCENEMANAGER_EXPORTS" /D "_MBCS" /FR /Fo".\..\obj\Release/" /GF /c
# ADD BASE MTL /nologo /win32
# ADD MTL /nologo /win32
# ADD BASE RSC /l 0x809 /d "NDEBUG"
# ADD RSC /l 0x809 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ogremain.lib /nologo /subsystem:console /dll /machine:I386 /def:"..\misc\DotSceneManager.def" /pdbtype:sept /libpath:"../../../../../ogrenew\Dependencies\lib\$(ConfigurationName)" /libpath:"../../../../../ogrenew\OgreMain\lib\$(ConfigurationName)"
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ogremain.lib /nologo /subsystem:console /dll /machine:I386 /def:"..\misc\DotSceneManager.def" /pdbtype:sept /libpath:"..\..\..\..\..\ogrenew\Dependencies\lib\release" /libpath:"..\..\..\..\..\ogrenew\OgreMain\lib\release"
# Begin Special Build Tool
OutDir=.\..\bin\release
SOURCE="$(InputPath)"
PostBuild_Cmds=copy $(OutDir)\$(TargetFileName) ..\..\..\..\..\ogrenew\Samples\Common\bin\release
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "Plugin_DotSceneManager - Win32 Debug"
# Name "Plugin_DotSceneManager - Win32 Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\src\DotSceneManager.cpp
DEP_CPP_DOTSC=\
	"..\..\..\..\..\ogrenew\OgreMain\include\OgreConfigFile.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h"\
	"..\include\DotSceneManager.h"\
	"..\include\SceneMain.h"\
	"..\include\SceneOctree.h"\
	"..\include\SceneOctreeRenderable.h"\
	{$(INCLUDE)}"config.h"\
	{$(INCLUDE)}"config\_epilog.h"\
	{$(INCLUDE)}"config\_msvc_warnings_off.h"\
	{$(INCLUDE)}"config\_prolog.h"\
	{$(INCLUDE)}"config\stl_apcc.h"\
	{$(INCLUDE)}"config\stl_apple.h"\
	{$(INCLUDE)}"config\stl_as400.h"\
	{$(INCLUDE)}"config\stl_bc.h"\
	{$(INCLUDE)}"config\stl_como.h"\
	{$(INCLUDE)}"config\stl_confix.h"\
	{$(INCLUDE)}"config\stl_cray.h"\
	{$(INCLUDE)}"config\stl_dec.h"\
	{$(INCLUDE)}"config\stl_dec_vms.h"\
	{$(INCLUDE)}"config\stl_dm.h"\
	{$(INCLUDE)}"config\stl_fujitsu.h"\
	{$(INCLUDE)}"config\stl_gcc.h"\
	{$(INCLUDE)}"config\stl_hpacc.h"\
	{$(INCLUDE)}"config\stl_ibm.h"\
	{$(INCLUDE)}"config\stl_icc.h"\
	{$(INCLUDE)}"config\stl_intel.h"\
	{$(INCLUDE)}"config\stl_kai.h"\
	{$(INCLUDE)}"config\stl_msvc.h"\
	{$(INCLUDE)}"config\stl_mwerks.h"\
	{$(INCLUDE)}"config\stl_mycomp.h"\
	{$(INCLUDE)}"config\stl_sco.h"\
	{$(INCLUDE)}"config\stl_select_lib.h"\
	{$(INCLUDE)}"config\stl_sgi.h"\
	{$(INCLUDE)}"config\stl_solaris.h"\
	{$(INCLUDE)}"config\stl_sunpro.h"\
	{$(INCLUDE)}"config\stl_symantec.h"\
	{$(INCLUDE)}"config\stl_watcom.h"\
	{$(INCLUDE)}"config\stl_wince.h"\
	{$(INCLUDE)}"config\stlcomp.h"\
	{$(INCLUDE)}"config\vc_select_lib.h"\
	{$(INCLUDE)}"hash_map"\
	{$(INCLUDE)}"hash_set"\
	{$(INCLUDE)}"Ogre.h"\
	{$(INCLUDE)}"OgreActionEvent.h"\
	{$(INCLUDE)}"OgreActionTarget.h"\
	{$(INCLUDE)}"OgreAnimation.h"\
	{$(INCLUDE)}"OgreAnimationState.h"\
	{$(INCLUDE)}"OgreAnimationTrack.h"\
	{$(INCLUDE)}"OgreArchiveEx.h"\
	{$(INCLUDE)}"OgreArchiveManager.h"\
	{$(INCLUDE)}"OgreAutoParamDataSource.h"\
	{$(INCLUDE)}"OgreAxisAlignedBox.h"\
	{$(INCLUDE)}"OgreBillboard.h"\
	{$(INCLUDE)}"OgreBillboardSet.h"\
	{$(INCLUDE)}"OgreBlendMode.h"\
	{$(INCLUDE)}"OgreBone.h"\
	{$(INCLUDE)}"OgreCamera.h"\
	{$(INCLUDE)}"OgreColourValue.h"\
	{$(INCLUDE)}"OgreCommon.h"\
	{$(INCLUDE)}"OgreConfig.h"\
	{$(INCLUDE)}"OgreConfigOptionMap.h"\
	{$(INCLUDE)}"OgreController.h"\
	{$(INCLUDE)}"OgreControllerManager.h"\
	{$(INCLUDE)}"OgreDataChunk.h"\
	{$(INCLUDE)}"OgreDynLibManager.h"\
	{$(INCLUDE)}"OgreEntity.h"\
	{$(INCLUDE)}"OgreEventDispatcher.h"\
	{$(INCLUDE)}"OgreEventListeners.h"\
	{$(INCLUDE)}"OgreEventProcessor.h"\
	{$(INCLUDE)}"OgreEventTarget.h"\
	{$(INCLUDE)}"OgreException.h"\
	{$(INCLUDE)}"OgreFrameListener.h"\
	{$(INCLUDE)}"OgreGpuProgram.h"\
	{$(INCLUDE)}"OgreGpuProgramManager.h"\
	{$(INCLUDE)}"OgreGuiContainer.h"\
	{$(INCLUDE)}"OgreGuiElement.h"\
	{$(INCLUDE)}"OgreGuiElementCommands.h"\
	{$(INCLUDE)}"OgreGuiManager.h"\
	{$(INCLUDE)}"OgreHardwareBuffer.h"\
	{$(INCLUDE)}"OgreHardwareBufferManager.h"\
	{$(INCLUDE)}"OgreHardwareIndexBuffer.h"\
	{$(INCLUDE)}"OgreHardwareVertexBuffer.h"\
	{$(INCLUDE)}"OgreImage.h"\
	{$(INCLUDE)}"OgreInput.h"\
	{$(INCLUDE)}"OgreInputEvent.h"\
	{$(INCLUDE)}"OgreIteratorWrappers.h"\
	{$(INCLUDE)}"OgreKeyFrame.h"\
	{$(INCLUDE)}"OgreKeyTarget.h"\
	{$(INCLUDE)}"OgreLight.h"\
	{$(INCLUDE)}"OgreLog.h"\
	{$(INCLUDE)}"OgreLogManager.h"\
	{$(INCLUDE)}"OgreMaterial.h"\
	{$(INCLUDE)}"OgreMaterialManager.h"\
	{$(INCLUDE)}"OgreMaterialSerializer.h"\
	{$(INCLUDE)}"OgreMath.h"\
	{$(INCLUDE)}"OgreMatrix3.h"\
	{$(INCLUDE)}"OgreMatrix4.h"\
	{$(INCLUDE)}"OgreMemoryMacros.h"\
	{$(INCLUDE)}"OgreMemoryManager.h"\
	{$(INCLUDE)}"OgreMesh.h"\
	{$(INCLUDE)}"OgreMeshManager.h"\
	{$(INCLUDE)}"OgreMeshSerializer.h"\
	{$(INCLUDE)}"OgreMeshSerializerImpl.h"\
	{$(INCLUDE)}"OgreMouseEvent.h"\
	{$(INCLUDE)}"OgreMouseMotionTarget.h"\
	{$(INCLUDE)}"OgreMouseTarget.h"\
	{$(INCLUDE)}"OgreMovableObject.h"\
	{$(INCLUDE)}"OgreNode.h"\
	{$(INCLUDE)}"OgreOverlay.h"\
	{$(INCLUDE)}"OgreOverlayManager.h"\
	{$(INCLUDE)}"OgreParticle.h"\
	{$(INCLUDE)}"OgreParticleAffector.h"\
	{$(INCLUDE)}"OgreParticleEmitter.h"\
	{$(INCLUDE)}"OgreParticleEmitterCommands.h"\
	{$(INCLUDE)}"OgreParticleIterator.h"\
	{$(INCLUDE)}"OgreParticleSystem.h"\
	{$(INCLUDE)}"OgreParticleSystemManager.h"\
	{$(INCLUDE)}"OgrePass.h"\
	{$(INCLUDE)}"OgrePatchMesh.h"\
	{$(INCLUDE)}"OgrePatchSurface.h"\
	{$(INCLUDE)}"OgrePlane.h"\
	{$(INCLUDE)}"OgrePlatform.h"\
	{$(INCLUDE)}"OgrePlatformManager.h"\
	{$(INCLUDE)}"OgrePositionTarget.h"\
	{$(INCLUDE)}"OgrePredefinedControllers.h"\
	{$(INCLUDE)}"OgrePrerequisites.h"\
	{$(INCLUDE)}"OgreProfiler.h"\
	{$(INCLUDE)}"OgreProgressiveMesh.h"\
	{$(INCLUDE)}"OgreQuaternion.h"\
	{$(INCLUDE)}"OgreRay.h"\
	{$(INCLUDE)}"OgreRenderable.h"\
	{$(INCLUDE)}"OgreRenderOperation.h"\
	{$(INCLUDE)}"OgreRenderQueue.h"\
	{$(INCLUDE)}"OgreRenderQueueListener.h"\
	{$(INCLUDE)}"OgreRenderSystem.h"\
	{$(INCLUDE)}"OgreRenderSystemCapabilities.h"\
	{$(INCLUDE)}"OgreRenderTarget.h"\
	{$(INCLUDE)}"OgreRenderTargetListener.h"\
	{$(INCLUDE)}"OgreRenderTexture.h"\
	{$(INCLUDE)}"OgreRenderWindow.h"\
	{$(INCLUDE)}"OgreResource.h"\
	{$(INCLUDE)}"OgreResourceManager.h"\
	{$(INCLUDE)}"OgreRoot.h"\
	{$(INCLUDE)}"OgreRotationalSpline.h"\
	{$(INCLUDE)}"OgreSceneManager.h"\
	{$(INCLUDE)}"OgreSceneManagerEnumerator.h"\
	{$(INCLUDE)}"OgreSceneNode.h"\
	{$(INCLUDE)}"OgreSceneQuery.h"\
	{$(INCLUDE)}"OgreScrollEvent.h"\
	{$(INCLUDE)}"OgreSDDataChunk.h"\
	{$(INCLUDE)}"OgreSerializer.h"\
	{$(INCLUDE)}"OgreSharedPtr.h"\
	{$(INCLUDE)}"OgreSimpleRenderable.h"\
	{$(INCLUDE)}"OgreSimpleSpline.h"\
	{$(INCLUDE)}"OgreSingleton.h"\
	{$(INCLUDE)}"OgreSkeleton.h"\
	{$(INCLUDE)}"OgreSkeletonManager.h"\
	{$(INCLUDE)}"OgreSkeletonSerializer.h"\
	{$(INCLUDE)}"OgreSphere.h"\
	{$(INCLUDE)}"OgreStdHeaders.h"\
	{$(INCLUDE)}"OgreString.h"\
	{$(INCLUDE)}"OgreStringConverter.h"\
	{$(INCLUDE)}"OgreStringInterface.h"\
	{$(INCLUDE)}"OgreStringVector.h"\
	{$(INCLUDE)}"OgreSubEntity.h"\
	{$(INCLUDE)}"OgreSubMesh.h"\
	{$(INCLUDE)}"OgreTargetManager.h"\
	{$(INCLUDE)}"OgreTechnique.h"\
	{$(INCLUDE)}"OgreTexture.h"\
	{$(INCLUDE)}"OgreTextureManager.h"\
	{$(INCLUDE)}"OgreTextureUnitState.h"\
	{$(INCLUDE)}"OgreTimer.h"\
	{$(INCLUDE)}"OgreUserDefinedObject.h"\
	{$(INCLUDE)}"OgreVector3.h"\
	{$(INCLUDE)}"OgreVector4.h"\
	{$(INCLUDE)}"OgreVertexBoneAssignment.h"\
	{$(INCLUDE)}"OgreVertexIndexData.h"\
	{$(INCLUDE)}"OgreViewport.h"\
	{$(INCLUDE)}"OS.h"\
	{$(INCLUDE)}"pthread.h"\
	{$(INCLUDE)}"stl\_abbrevs.h"\
	{$(INCLUDE)}"stl\_algobase.c"\
	{$(INCLUDE)}"stl\_algobase.h"\
	{$(INCLUDE)}"stl\_alloc.c"\
	{$(INCLUDE)}"stl\_alloc.h"\
	{$(INCLUDE)}"stl\_alloc_old.h"\
	{$(INCLUDE)}"stl\_bvector.h"\
	{$(INCLUDE)}"stl\_config.h"\
	{$(INCLUDE)}"stl\_config_compat.h"\
	{$(INCLUDE)}"stl\_config_compat_post.h"\
	{$(INCLUDE)}"stl\_construct.h"\
	{$(INCLUDE)}"stl\_cwchar.h"\
	{$(INCLUDE)}"stl\_epilog.h"\
	{$(INCLUDE)}"stl\_function_base.h"\
	{$(INCLUDE)}"stl\_hash_fun.h"\
	{$(INCLUDE)}"stl\_hash_map.h"\
	{$(INCLUDE)}"stl\_hash_set.h"\
	{$(INCLUDE)}"stl\_hashtable.c"\
	{$(INCLUDE)}"stl\_hashtable.h"\
	{$(INCLUDE)}"stl\_iterator.h"\
	{$(INCLUDE)}"stl\_iterator_base.h"\
	{$(INCLUDE)}"stl\_iterator_old.h"\
	{$(INCLUDE)}"stl\_new.h"\
	{$(INCLUDE)}"stl\_pair.h"\
	{$(INCLUDE)}"stl\_prolog.h"\
	{$(INCLUDE)}"stl\_pthread_alloc.c"\
	{$(INCLUDE)}"stl\_pthread_alloc.h"\
	{$(INCLUDE)}"stl\_ptrs_specialize.h"\
	{$(INCLUDE)}"stl\_range_errors.h"\
	{$(INCLUDE)}"stl\_relops_cont.h"\
	{$(INCLUDE)}"stl\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\_site_config.h"\
	{$(INCLUDE)}"stl\_sparc_atomic.h"\
	{$(INCLUDE)}"stl\_threads.c"\
	{$(INCLUDE)}"stl\_threads.h"\
	{$(INCLUDE)}"stl\_uninitialized.h"\
	{$(INCLUDE)}"stl\_vector.c"\
	{$(INCLUDE)}"stl\_vector.h"\
	{$(INCLUDE)}"stl\debug\_debug.c"\
	{$(INCLUDE)}"stl\debug\_debug.h"\
	{$(INCLUDE)}"stl\debug\_hashtable.h"\
	{$(INCLUDE)}"stl\debug\_iterator.h"\
	{$(INCLUDE)}"stl\debug\_relops_cont.h"\
	{$(INCLUDE)}"stl\debug\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\debug\_vector.h"\
	{$(INCLUDE)}"stl\type_traits.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_map.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_set.h"\
	{$(INCLUDE)}"stl\wrappers\_vector.h"\
	{$(INCLUDE)}"stl_user_config.h"\
	
NODEP_CPP_DOTSC=\
	"..\..\..\..\..\..\..\usr\include\pthread.h"\
	
# End Source File
# Begin Source File

SOURCE=..\src\DotSceneManagerDLL.cpp
DEP_CPP_DOTSCE=\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h"\
	"..\include\DotSceneManager.h"\
	"..\include\SceneMain.h"\
	"..\include\SceneOctree.h"\
	"..\include\SceneOctreeRenderable.h"\
	{$(INCLUDE)}"config.h"\
	{$(INCLUDE)}"config\_epilog.h"\
	{$(INCLUDE)}"config\_msvc_warnings_off.h"\
	{$(INCLUDE)}"config\_prolog.h"\
	{$(INCLUDE)}"config\stl_apcc.h"\
	{$(INCLUDE)}"config\stl_apple.h"\
	{$(INCLUDE)}"config\stl_as400.h"\
	{$(INCLUDE)}"config\stl_bc.h"\
	{$(INCLUDE)}"config\stl_como.h"\
	{$(INCLUDE)}"config\stl_confix.h"\
	{$(INCLUDE)}"config\stl_cray.h"\
	{$(INCLUDE)}"config\stl_dec.h"\
	{$(INCLUDE)}"config\stl_dec_vms.h"\
	{$(INCLUDE)}"config\stl_dm.h"\
	{$(INCLUDE)}"config\stl_fujitsu.h"\
	{$(INCLUDE)}"config\stl_gcc.h"\
	{$(INCLUDE)}"config\stl_hpacc.h"\
	{$(INCLUDE)}"config\stl_ibm.h"\
	{$(INCLUDE)}"config\stl_icc.h"\
	{$(INCLUDE)}"config\stl_intel.h"\
	{$(INCLUDE)}"config\stl_kai.h"\
	{$(INCLUDE)}"config\stl_msvc.h"\
	{$(INCLUDE)}"config\stl_mwerks.h"\
	{$(INCLUDE)}"config\stl_mycomp.h"\
	{$(INCLUDE)}"config\stl_sco.h"\
	{$(INCLUDE)}"config\stl_select_lib.h"\
	{$(INCLUDE)}"config\stl_sgi.h"\
	{$(INCLUDE)}"config\stl_solaris.h"\
	{$(INCLUDE)}"config\stl_sunpro.h"\
	{$(INCLUDE)}"config\stl_symantec.h"\
	{$(INCLUDE)}"config\stl_watcom.h"\
	{$(INCLUDE)}"config\stl_wince.h"\
	{$(INCLUDE)}"config\stlcomp.h"\
	{$(INCLUDE)}"config\vc_select_lib.h"\
	{$(INCLUDE)}"hash_map"\
	{$(INCLUDE)}"hash_set"\
	{$(INCLUDE)}"Ogre.h"\
	{$(INCLUDE)}"OgreActionEvent.h"\
	{$(INCLUDE)}"OgreActionTarget.h"\
	{$(INCLUDE)}"OgreAnimation.h"\
	{$(INCLUDE)}"OgreAnimationState.h"\
	{$(INCLUDE)}"OgreAnimationTrack.h"\
	{$(INCLUDE)}"OgreArchiveEx.h"\
	{$(INCLUDE)}"OgreArchiveManager.h"\
	{$(INCLUDE)}"OgreAutoParamDataSource.h"\
	{$(INCLUDE)}"OgreAxisAlignedBox.h"\
	{$(INCLUDE)}"OgreBillboard.h"\
	{$(INCLUDE)}"OgreBillboardSet.h"\
	{$(INCLUDE)}"OgreBlendMode.h"\
	{$(INCLUDE)}"OgreBone.h"\
	{$(INCLUDE)}"OgreCamera.h"\
	{$(INCLUDE)}"OgreColourValue.h"\
	{$(INCLUDE)}"OgreCommon.h"\
	{$(INCLUDE)}"OgreConfig.h"\
	{$(INCLUDE)}"OgreConfigOptionMap.h"\
	{$(INCLUDE)}"OgreController.h"\
	{$(INCLUDE)}"OgreControllerManager.h"\
	{$(INCLUDE)}"OgreDataChunk.h"\
	{$(INCLUDE)}"OgreDynLibManager.h"\
	{$(INCLUDE)}"OgreEntity.h"\
	{$(INCLUDE)}"OgreEventDispatcher.h"\
	{$(INCLUDE)}"OgreEventListeners.h"\
	{$(INCLUDE)}"OgreEventProcessor.h"\
	{$(INCLUDE)}"OgreEventTarget.h"\
	{$(INCLUDE)}"OgreException.h"\
	{$(INCLUDE)}"OgreFrameListener.h"\
	{$(INCLUDE)}"OgreGpuProgram.h"\
	{$(INCLUDE)}"OgreGpuProgramManager.h"\
	{$(INCLUDE)}"OgreGuiContainer.h"\
	{$(INCLUDE)}"OgreGuiElement.h"\
	{$(INCLUDE)}"OgreGuiElementCommands.h"\
	{$(INCLUDE)}"OgreGuiManager.h"\
	{$(INCLUDE)}"OgreHardwareBuffer.h"\
	{$(INCLUDE)}"OgreHardwareBufferManager.h"\
	{$(INCLUDE)}"OgreHardwareIndexBuffer.h"\
	{$(INCLUDE)}"OgreHardwareVertexBuffer.h"\
	{$(INCLUDE)}"OgreImage.h"\
	{$(INCLUDE)}"OgreInput.h"\
	{$(INCLUDE)}"OgreInputEvent.h"\
	{$(INCLUDE)}"OgreIteratorWrappers.h"\
	{$(INCLUDE)}"OgreKeyFrame.h"\
	{$(INCLUDE)}"OgreKeyTarget.h"\
	{$(INCLUDE)}"OgreLight.h"\
	{$(INCLUDE)}"OgreLog.h"\
	{$(INCLUDE)}"OgreLogManager.h"\
	{$(INCLUDE)}"OgreMaterial.h"\
	{$(INCLUDE)}"OgreMaterialManager.h"\
	{$(INCLUDE)}"OgreMaterialSerializer.h"\
	{$(INCLUDE)}"OgreMath.h"\
	{$(INCLUDE)}"OgreMatrix3.h"\
	{$(INCLUDE)}"OgreMatrix4.h"\
	{$(INCLUDE)}"OgreMemoryMacros.h"\
	{$(INCLUDE)}"OgreMemoryManager.h"\
	{$(INCLUDE)}"OgreMesh.h"\
	{$(INCLUDE)}"OgreMeshManager.h"\
	{$(INCLUDE)}"OgreMeshSerializer.h"\
	{$(INCLUDE)}"OgreMeshSerializerImpl.h"\
	{$(INCLUDE)}"OgreMouseEvent.h"\
	{$(INCLUDE)}"OgreMouseMotionTarget.h"\
	{$(INCLUDE)}"OgreMouseTarget.h"\
	{$(INCLUDE)}"OgreMovableObject.h"\
	{$(INCLUDE)}"OgreNode.h"\
	{$(INCLUDE)}"OgreOverlay.h"\
	{$(INCLUDE)}"OgreOverlayManager.h"\
	{$(INCLUDE)}"OgreParticle.h"\
	{$(INCLUDE)}"OgreParticleAffector.h"\
	{$(INCLUDE)}"OgreParticleEmitter.h"\
	{$(INCLUDE)}"OgreParticleEmitterCommands.h"\
	{$(INCLUDE)}"OgreParticleIterator.h"\
	{$(INCLUDE)}"OgreParticleSystem.h"\
	{$(INCLUDE)}"OgreParticleSystemManager.h"\
	{$(INCLUDE)}"OgrePass.h"\
	{$(INCLUDE)}"OgrePatchMesh.h"\
	{$(INCLUDE)}"OgrePatchSurface.h"\
	{$(INCLUDE)}"OgrePlane.h"\
	{$(INCLUDE)}"OgrePlatform.h"\
	{$(INCLUDE)}"OgrePlatformManager.h"\
	{$(INCLUDE)}"OgrePositionTarget.h"\
	{$(INCLUDE)}"OgrePredefinedControllers.h"\
	{$(INCLUDE)}"OgrePrerequisites.h"\
	{$(INCLUDE)}"OgreProfiler.h"\
	{$(INCLUDE)}"OgreProgressiveMesh.h"\
	{$(INCLUDE)}"OgreQuaternion.h"\
	{$(INCLUDE)}"OgreRay.h"\
	{$(INCLUDE)}"OgreRenderable.h"\
	{$(INCLUDE)}"OgreRenderOperation.h"\
	{$(INCLUDE)}"OgreRenderQueue.h"\
	{$(INCLUDE)}"OgreRenderQueueListener.h"\
	{$(INCLUDE)}"OgreRenderSystem.h"\
	{$(INCLUDE)}"OgreRenderSystemCapabilities.h"\
	{$(INCLUDE)}"OgreRenderTarget.h"\
	{$(INCLUDE)}"OgreRenderTargetListener.h"\
	{$(INCLUDE)}"OgreRenderTexture.h"\
	{$(INCLUDE)}"OgreRenderWindow.h"\
	{$(INCLUDE)}"OgreResource.h"\
	{$(INCLUDE)}"OgreResourceManager.h"\
	{$(INCLUDE)}"OgreRoot.h"\
	{$(INCLUDE)}"OgreRotationalSpline.h"\
	{$(INCLUDE)}"OgreSceneManager.h"\
	{$(INCLUDE)}"OgreSceneManagerEnumerator.h"\
	{$(INCLUDE)}"OgreSceneNode.h"\
	{$(INCLUDE)}"OgreSceneQuery.h"\
	{$(INCLUDE)}"OgreScrollEvent.h"\
	{$(INCLUDE)}"OgreSDDataChunk.h"\
	{$(INCLUDE)}"OgreSerializer.h"\
	{$(INCLUDE)}"OgreSharedPtr.h"\
	{$(INCLUDE)}"OgreSimpleRenderable.h"\
	{$(INCLUDE)}"OgreSimpleSpline.h"\
	{$(INCLUDE)}"OgreSingleton.h"\
	{$(INCLUDE)}"OgreSkeleton.h"\
	{$(INCLUDE)}"OgreSkeletonManager.h"\
	{$(INCLUDE)}"OgreSkeletonSerializer.h"\
	{$(INCLUDE)}"OgreSphere.h"\
	{$(INCLUDE)}"OgreStdHeaders.h"\
	{$(INCLUDE)}"OgreString.h"\
	{$(INCLUDE)}"OgreStringConverter.h"\
	{$(INCLUDE)}"OgreStringInterface.h"\
	{$(INCLUDE)}"OgreStringVector.h"\
	{$(INCLUDE)}"OgreSubEntity.h"\
	{$(INCLUDE)}"OgreSubMesh.h"\
	{$(INCLUDE)}"OgreTargetManager.h"\
	{$(INCLUDE)}"OgreTechnique.h"\
	{$(INCLUDE)}"OgreTexture.h"\
	{$(INCLUDE)}"OgreTextureManager.h"\
	{$(INCLUDE)}"OgreTextureUnitState.h"\
	{$(INCLUDE)}"OgreTimer.h"\
	{$(INCLUDE)}"OgreUserDefinedObject.h"\
	{$(INCLUDE)}"OgreVector3.h"\
	{$(INCLUDE)}"OgreVector4.h"\
	{$(INCLUDE)}"OgreVertexBoneAssignment.h"\
	{$(INCLUDE)}"OgreVertexIndexData.h"\
	{$(INCLUDE)}"OgreViewport.h"\
	{$(INCLUDE)}"OS.h"\
	{$(INCLUDE)}"pthread.h"\
	{$(INCLUDE)}"stl\_abbrevs.h"\
	{$(INCLUDE)}"stl\_algobase.c"\
	{$(INCLUDE)}"stl\_algobase.h"\
	{$(INCLUDE)}"stl\_alloc.c"\
	{$(INCLUDE)}"stl\_alloc.h"\
	{$(INCLUDE)}"stl\_alloc_old.h"\
	{$(INCLUDE)}"stl\_bvector.h"\
	{$(INCLUDE)}"stl\_config.h"\
	{$(INCLUDE)}"stl\_config_compat.h"\
	{$(INCLUDE)}"stl\_config_compat_post.h"\
	{$(INCLUDE)}"stl\_construct.h"\
	{$(INCLUDE)}"stl\_cwchar.h"\
	{$(INCLUDE)}"stl\_epilog.h"\
	{$(INCLUDE)}"stl\_function_base.h"\
	{$(INCLUDE)}"stl\_hash_fun.h"\
	{$(INCLUDE)}"stl\_hash_map.h"\
	{$(INCLUDE)}"stl\_hash_set.h"\
	{$(INCLUDE)}"stl\_hashtable.c"\
	{$(INCLUDE)}"stl\_hashtable.h"\
	{$(INCLUDE)}"stl\_iterator.h"\
	{$(INCLUDE)}"stl\_iterator_base.h"\
	{$(INCLUDE)}"stl\_iterator_old.h"\
	{$(INCLUDE)}"stl\_new.h"\
	{$(INCLUDE)}"stl\_pair.h"\
	{$(INCLUDE)}"stl\_prolog.h"\
	{$(INCLUDE)}"stl\_pthread_alloc.c"\
	{$(INCLUDE)}"stl\_pthread_alloc.h"\
	{$(INCLUDE)}"stl\_ptrs_specialize.h"\
	{$(INCLUDE)}"stl\_range_errors.h"\
	{$(INCLUDE)}"stl\_relops_cont.h"\
	{$(INCLUDE)}"stl\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\_site_config.h"\
	{$(INCLUDE)}"stl\_sparc_atomic.h"\
	{$(INCLUDE)}"stl\_threads.c"\
	{$(INCLUDE)}"stl\_threads.h"\
	{$(INCLUDE)}"stl\_uninitialized.h"\
	{$(INCLUDE)}"stl\_vector.c"\
	{$(INCLUDE)}"stl\_vector.h"\
	{$(INCLUDE)}"stl\debug\_debug.c"\
	{$(INCLUDE)}"stl\debug\_debug.h"\
	{$(INCLUDE)}"stl\debug\_hashtable.h"\
	{$(INCLUDE)}"stl\debug\_iterator.h"\
	{$(INCLUDE)}"stl\debug\_relops_cont.h"\
	{$(INCLUDE)}"stl\debug\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\debug\_vector.h"\
	{$(INCLUDE)}"stl\type_traits.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_map.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_set.h"\
	{$(INCLUDE)}"stl\wrappers\_vector.h"\
	{$(INCLUDE)}"stl_user_config.h"\
	
NODEP_CPP_DOTSCE=\
	"..\..\..\..\..\..\..\usr\include\pthread.h"\
	
# End Source File
# Begin Source File

SOURCE=..\src\SceneMain.cpp
DEP_CPP_SCENE=\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h"\
	"..\include\SceneMain.h"\
	"..\include\SceneOctree.h"\
	"..\include\SceneOctreeRenderable.h"\
	{$(INCLUDE)}"config.h"\
	{$(INCLUDE)}"config\_epilog.h"\
	{$(INCLUDE)}"config\_msvc_warnings_off.h"\
	{$(INCLUDE)}"config\_prolog.h"\
	{$(INCLUDE)}"config\stl_apcc.h"\
	{$(INCLUDE)}"config\stl_apple.h"\
	{$(INCLUDE)}"config\stl_as400.h"\
	{$(INCLUDE)}"config\stl_bc.h"\
	{$(INCLUDE)}"config\stl_como.h"\
	{$(INCLUDE)}"config\stl_confix.h"\
	{$(INCLUDE)}"config\stl_cray.h"\
	{$(INCLUDE)}"config\stl_dec.h"\
	{$(INCLUDE)}"config\stl_dec_vms.h"\
	{$(INCLUDE)}"config\stl_dm.h"\
	{$(INCLUDE)}"config\stl_fujitsu.h"\
	{$(INCLUDE)}"config\stl_gcc.h"\
	{$(INCLUDE)}"config\stl_hpacc.h"\
	{$(INCLUDE)}"config\stl_ibm.h"\
	{$(INCLUDE)}"config\stl_icc.h"\
	{$(INCLUDE)}"config\stl_intel.h"\
	{$(INCLUDE)}"config\stl_kai.h"\
	{$(INCLUDE)}"config\stl_msvc.h"\
	{$(INCLUDE)}"config\stl_mwerks.h"\
	{$(INCLUDE)}"config\stl_mycomp.h"\
	{$(INCLUDE)}"config\stl_sco.h"\
	{$(INCLUDE)}"config\stl_select_lib.h"\
	{$(INCLUDE)}"config\stl_sgi.h"\
	{$(INCLUDE)}"config\stl_solaris.h"\
	{$(INCLUDE)}"config\stl_sunpro.h"\
	{$(INCLUDE)}"config\stl_symantec.h"\
	{$(INCLUDE)}"config\stl_watcom.h"\
	{$(INCLUDE)}"config\stl_wince.h"\
	{$(INCLUDE)}"config\stlcomp.h"\
	{$(INCLUDE)}"config\vc_select_lib.h"\
	{$(INCLUDE)}"hash_map"\
	{$(INCLUDE)}"hash_set"\
	{$(INCLUDE)}"Ogre.h"\
	{$(INCLUDE)}"OgreActionEvent.h"\
	{$(INCLUDE)}"OgreActionTarget.h"\
	{$(INCLUDE)}"OgreAnimation.h"\
	{$(INCLUDE)}"OgreAnimationState.h"\
	{$(INCLUDE)}"OgreAnimationTrack.h"\
	{$(INCLUDE)}"OgreArchiveEx.h"\
	{$(INCLUDE)}"OgreArchiveManager.h"\
	{$(INCLUDE)}"OgreAutoParamDataSource.h"\
	{$(INCLUDE)}"OgreAxisAlignedBox.h"\
	{$(INCLUDE)}"OgreBillboard.h"\
	{$(INCLUDE)}"OgreBillboardSet.h"\
	{$(INCLUDE)}"OgreBlendMode.h"\
	{$(INCLUDE)}"OgreBone.h"\
	{$(INCLUDE)}"OgreCamera.h"\
	{$(INCLUDE)}"OgreColourValue.h"\
	{$(INCLUDE)}"OgreCommon.h"\
	{$(INCLUDE)}"OgreConfig.h"\
	{$(INCLUDE)}"OgreConfigOptionMap.h"\
	{$(INCLUDE)}"OgreController.h"\
	{$(INCLUDE)}"OgreControllerManager.h"\
	{$(INCLUDE)}"OgreDataChunk.h"\
	{$(INCLUDE)}"OgreDynLibManager.h"\
	{$(INCLUDE)}"OgreEntity.h"\
	{$(INCLUDE)}"OgreEventDispatcher.h"\
	{$(INCLUDE)}"OgreEventListeners.h"\
	{$(INCLUDE)}"OgreEventProcessor.h"\
	{$(INCLUDE)}"OgreEventTarget.h"\
	{$(INCLUDE)}"OgreException.h"\
	{$(INCLUDE)}"OgreFrameListener.h"\
	{$(INCLUDE)}"OgreGpuProgram.h"\
	{$(INCLUDE)}"OgreGpuProgramManager.h"\
	{$(INCLUDE)}"OgreGuiContainer.h"\
	{$(INCLUDE)}"OgreGuiElement.h"\
	{$(INCLUDE)}"OgreGuiElementCommands.h"\
	{$(INCLUDE)}"OgreGuiManager.h"\
	{$(INCLUDE)}"OgreHardwareBuffer.h"\
	{$(INCLUDE)}"OgreHardwareBufferManager.h"\
	{$(INCLUDE)}"OgreHardwareIndexBuffer.h"\
	{$(INCLUDE)}"OgreHardwareVertexBuffer.h"\
	{$(INCLUDE)}"OgreImage.h"\
	{$(INCLUDE)}"OgreInput.h"\
	{$(INCLUDE)}"OgreInputEvent.h"\
	{$(INCLUDE)}"OgreIteratorWrappers.h"\
	{$(INCLUDE)}"OgreKeyFrame.h"\
	{$(INCLUDE)}"OgreKeyTarget.h"\
	{$(INCLUDE)}"OgreLight.h"\
	{$(INCLUDE)}"OgreLog.h"\
	{$(INCLUDE)}"OgreLogManager.h"\
	{$(INCLUDE)}"OgreMaterial.h"\
	{$(INCLUDE)}"OgreMaterialManager.h"\
	{$(INCLUDE)}"OgreMaterialSerializer.h"\
	{$(INCLUDE)}"OgreMath.h"\
	{$(INCLUDE)}"OgreMatrix3.h"\
	{$(INCLUDE)}"OgreMatrix4.h"\
	{$(INCLUDE)}"OgreMemoryMacros.h"\
	{$(INCLUDE)}"OgreMemoryManager.h"\
	{$(INCLUDE)}"OgreMesh.h"\
	{$(INCLUDE)}"OgreMeshManager.h"\
	{$(INCLUDE)}"OgreMeshSerializer.h"\
	{$(INCLUDE)}"OgreMeshSerializerImpl.h"\
	{$(INCLUDE)}"OgreMouseEvent.h"\
	{$(INCLUDE)}"OgreMouseMotionTarget.h"\
	{$(INCLUDE)}"OgreMouseTarget.h"\
	{$(INCLUDE)}"OgreMovableObject.h"\
	{$(INCLUDE)}"OgreNode.h"\
	{$(INCLUDE)}"OgreOverlay.h"\
	{$(INCLUDE)}"OgreOverlayManager.h"\
	{$(INCLUDE)}"OgreParticle.h"\
	{$(INCLUDE)}"OgreParticleAffector.h"\
	{$(INCLUDE)}"OgreParticleEmitter.h"\
	{$(INCLUDE)}"OgreParticleEmitterCommands.h"\
	{$(INCLUDE)}"OgreParticleIterator.h"\
	{$(INCLUDE)}"OgreParticleSystem.h"\
	{$(INCLUDE)}"OgreParticleSystemManager.h"\
	{$(INCLUDE)}"OgrePass.h"\
	{$(INCLUDE)}"OgrePatchMesh.h"\
	{$(INCLUDE)}"OgrePatchSurface.h"\
	{$(INCLUDE)}"OgrePlane.h"\
	{$(INCLUDE)}"OgrePlatform.h"\
	{$(INCLUDE)}"OgrePlatformManager.h"\
	{$(INCLUDE)}"OgrePositionTarget.h"\
	{$(INCLUDE)}"OgrePredefinedControllers.h"\
	{$(INCLUDE)}"OgrePrerequisites.h"\
	{$(INCLUDE)}"OgreProfiler.h"\
	{$(INCLUDE)}"OgreProgressiveMesh.h"\
	{$(INCLUDE)}"OgreQuaternion.h"\
	{$(INCLUDE)}"OgreRay.h"\
	{$(INCLUDE)}"OgreRenderable.h"\
	{$(INCLUDE)}"OgreRenderOperation.h"\
	{$(INCLUDE)}"OgreRenderQueue.h"\
	{$(INCLUDE)}"OgreRenderQueueListener.h"\
	{$(INCLUDE)}"OgreRenderSystem.h"\
	{$(INCLUDE)}"OgreRenderSystemCapabilities.h"\
	{$(INCLUDE)}"OgreRenderTarget.h"\
	{$(INCLUDE)}"OgreRenderTargetListener.h"\
	{$(INCLUDE)}"OgreRenderTexture.h"\
	{$(INCLUDE)}"OgreRenderWindow.h"\
	{$(INCLUDE)}"OgreResource.h"\
	{$(INCLUDE)}"OgreResourceManager.h"\
	{$(INCLUDE)}"OgreRoot.h"\
	{$(INCLUDE)}"OgreRotationalSpline.h"\
	{$(INCLUDE)}"OgreSceneManager.h"\
	{$(INCLUDE)}"OgreSceneManagerEnumerator.h"\
	{$(INCLUDE)}"OgreSceneNode.h"\
	{$(INCLUDE)}"OgreSceneQuery.h"\
	{$(INCLUDE)}"OgreScrollEvent.h"\
	{$(INCLUDE)}"OgreSDDataChunk.h"\
	{$(INCLUDE)}"OgreSerializer.h"\
	{$(INCLUDE)}"OgreSharedPtr.h"\
	{$(INCLUDE)}"OgreSimpleRenderable.h"\
	{$(INCLUDE)}"OgreSimpleSpline.h"\
	{$(INCLUDE)}"OgreSingleton.h"\
	{$(INCLUDE)}"OgreSkeleton.h"\
	{$(INCLUDE)}"OgreSkeletonManager.h"\
	{$(INCLUDE)}"OgreSkeletonSerializer.h"\
	{$(INCLUDE)}"OgreSphere.h"\
	{$(INCLUDE)}"OgreStdHeaders.h"\
	{$(INCLUDE)}"OgreString.h"\
	{$(INCLUDE)}"OgreStringConverter.h"\
	{$(INCLUDE)}"OgreStringInterface.h"\
	{$(INCLUDE)}"OgreStringVector.h"\
	{$(INCLUDE)}"OgreSubEntity.h"\
	{$(INCLUDE)}"OgreSubMesh.h"\
	{$(INCLUDE)}"OgreTargetManager.h"\
	{$(INCLUDE)}"OgreTechnique.h"\
	{$(INCLUDE)}"OgreTexture.h"\
	{$(INCLUDE)}"OgreTextureManager.h"\
	{$(INCLUDE)}"OgreTextureUnitState.h"\
	{$(INCLUDE)}"OgreTimer.h"\
	{$(INCLUDE)}"OgreUserDefinedObject.h"\
	{$(INCLUDE)}"OgreVector3.h"\
	{$(INCLUDE)}"OgreVector4.h"\
	{$(INCLUDE)}"OgreVertexBoneAssignment.h"\
	{$(INCLUDE)}"OgreVertexIndexData.h"\
	{$(INCLUDE)}"OgreViewport.h"\
	{$(INCLUDE)}"OS.h"\
	{$(INCLUDE)}"pthread.h"\
	{$(INCLUDE)}"stl\_abbrevs.h"\
	{$(INCLUDE)}"stl\_algobase.c"\
	{$(INCLUDE)}"stl\_algobase.h"\
	{$(INCLUDE)}"stl\_alloc.c"\
	{$(INCLUDE)}"stl\_alloc.h"\
	{$(INCLUDE)}"stl\_alloc_old.h"\
	{$(INCLUDE)}"stl\_bvector.h"\
	{$(INCLUDE)}"stl\_config.h"\
	{$(INCLUDE)}"stl\_config_compat.h"\
	{$(INCLUDE)}"stl\_config_compat_post.h"\
	{$(INCLUDE)}"stl\_construct.h"\
	{$(INCLUDE)}"stl\_cwchar.h"\
	{$(INCLUDE)}"stl\_epilog.h"\
	{$(INCLUDE)}"stl\_function_base.h"\
	{$(INCLUDE)}"stl\_hash_fun.h"\
	{$(INCLUDE)}"stl\_hash_map.h"\
	{$(INCLUDE)}"stl\_hash_set.h"\
	{$(INCLUDE)}"stl\_hashtable.c"\
	{$(INCLUDE)}"stl\_hashtable.h"\
	{$(INCLUDE)}"stl\_iterator.h"\
	{$(INCLUDE)}"stl\_iterator_base.h"\
	{$(INCLUDE)}"stl\_iterator_old.h"\
	{$(INCLUDE)}"stl\_new.h"\
	{$(INCLUDE)}"stl\_pair.h"\
	{$(INCLUDE)}"stl\_prolog.h"\
	{$(INCLUDE)}"stl\_pthread_alloc.c"\
	{$(INCLUDE)}"stl\_pthread_alloc.h"\
	{$(INCLUDE)}"stl\_ptrs_specialize.h"\
	{$(INCLUDE)}"stl\_range_errors.h"\
	{$(INCLUDE)}"stl\_relops_cont.h"\
	{$(INCLUDE)}"stl\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\_site_config.h"\
	{$(INCLUDE)}"stl\_sparc_atomic.h"\
	{$(INCLUDE)}"stl\_threads.c"\
	{$(INCLUDE)}"stl\_threads.h"\
	{$(INCLUDE)}"stl\_uninitialized.h"\
	{$(INCLUDE)}"stl\_vector.c"\
	{$(INCLUDE)}"stl\_vector.h"\
	{$(INCLUDE)}"stl\debug\_debug.c"\
	{$(INCLUDE)}"stl\debug\_debug.h"\
	{$(INCLUDE)}"stl\debug\_hashtable.h"\
	{$(INCLUDE)}"stl\debug\_iterator.h"\
	{$(INCLUDE)}"stl\debug\_relops_cont.h"\
	{$(INCLUDE)}"stl\debug\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\debug\_vector.h"\
	{$(INCLUDE)}"stl\type_traits.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_map.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_set.h"\
	{$(INCLUDE)}"stl\wrappers\_vector.h"\
	{$(INCLUDE)}"stl_user_config.h"\
	
NODEP_CPP_SCENE=\
	"..\..\..\..\..\..\..\usr\include\pthread.h"\
	
# End Source File
# Begin Source File

SOURCE=..\src\SceneOctree.cpp
DEP_CPP_SCENEO=\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h"\
	"..\include\SceneOctree.h"\
	"..\include\SceneOctreeRenderable.h"\
	{$(INCLUDE)}"config.h"\
	{$(INCLUDE)}"config\_epilog.h"\
	{$(INCLUDE)}"config\_msvc_warnings_off.h"\
	{$(INCLUDE)}"config\_prolog.h"\
	{$(INCLUDE)}"config\stl_apcc.h"\
	{$(INCLUDE)}"config\stl_apple.h"\
	{$(INCLUDE)}"config\stl_as400.h"\
	{$(INCLUDE)}"config\stl_bc.h"\
	{$(INCLUDE)}"config\stl_como.h"\
	{$(INCLUDE)}"config\stl_confix.h"\
	{$(INCLUDE)}"config\stl_cray.h"\
	{$(INCLUDE)}"config\stl_dec.h"\
	{$(INCLUDE)}"config\stl_dec_vms.h"\
	{$(INCLUDE)}"config\stl_dm.h"\
	{$(INCLUDE)}"config\stl_fujitsu.h"\
	{$(INCLUDE)}"config\stl_gcc.h"\
	{$(INCLUDE)}"config\stl_hpacc.h"\
	{$(INCLUDE)}"config\stl_ibm.h"\
	{$(INCLUDE)}"config\stl_icc.h"\
	{$(INCLUDE)}"config\stl_intel.h"\
	{$(INCLUDE)}"config\stl_kai.h"\
	{$(INCLUDE)}"config\stl_msvc.h"\
	{$(INCLUDE)}"config\stl_mwerks.h"\
	{$(INCLUDE)}"config\stl_mycomp.h"\
	{$(INCLUDE)}"config\stl_sco.h"\
	{$(INCLUDE)}"config\stl_select_lib.h"\
	{$(INCLUDE)}"config\stl_sgi.h"\
	{$(INCLUDE)}"config\stl_solaris.h"\
	{$(INCLUDE)}"config\stl_sunpro.h"\
	{$(INCLUDE)}"config\stl_symantec.h"\
	{$(INCLUDE)}"config\stl_watcom.h"\
	{$(INCLUDE)}"config\stl_wince.h"\
	{$(INCLUDE)}"config\stlcomp.h"\
	{$(INCLUDE)}"config\vc_select_lib.h"\
	{$(INCLUDE)}"hash_map"\
	{$(INCLUDE)}"hash_set"\
	{$(INCLUDE)}"Ogre.h"\
	{$(INCLUDE)}"OgreActionEvent.h"\
	{$(INCLUDE)}"OgreActionTarget.h"\
	{$(INCLUDE)}"OgreAnimation.h"\
	{$(INCLUDE)}"OgreAnimationState.h"\
	{$(INCLUDE)}"OgreAnimationTrack.h"\
	{$(INCLUDE)}"OgreArchiveEx.h"\
	{$(INCLUDE)}"OgreArchiveManager.h"\
	{$(INCLUDE)}"OgreAutoParamDataSource.h"\
	{$(INCLUDE)}"OgreAxisAlignedBox.h"\
	{$(INCLUDE)}"OgreBillboard.h"\
	{$(INCLUDE)}"OgreBillboardSet.h"\
	{$(INCLUDE)}"OgreBlendMode.h"\
	{$(INCLUDE)}"OgreBone.h"\
	{$(INCLUDE)}"OgreCamera.h"\
	{$(INCLUDE)}"OgreColourValue.h"\
	{$(INCLUDE)}"OgreCommon.h"\
	{$(INCLUDE)}"OgreConfig.h"\
	{$(INCLUDE)}"OgreConfigOptionMap.h"\
	{$(INCLUDE)}"OgreController.h"\
	{$(INCLUDE)}"OgreControllerManager.h"\
	{$(INCLUDE)}"OgreDataChunk.h"\
	{$(INCLUDE)}"OgreDynLibManager.h"\
	{$(INCLUDE)}"OgreEntity.h"\
	{$(INCLUDE)}"OgreEventDispatcher.h"\
	{$(INCLUDE)}"OgreEventListeners.h"\
	{$(INCLUDE)}"OgreEventProcessor.h"\
	{$(INCLUDE)}"OgreEventTarget.h"\
	{$(INCLUDE)}"OgreException.h"\
	{$(INCLUDE)}"OgreFrameListener.h"\
	{$(INCLUDE)}"OgreGpuProgram.h"\
	{$(INCLUDE)}"OgreGpuProgramManager.h"\
	{$(INCLUDE)}"OgreGuiContainer.h"\
	{$(INCLUDE)}"OgreGuiElement.h"\
	{$(INCLUDE)}"OgreGuiElementCommands.h"\
	{$(INCLUDE)}"OgreGuiManager.h"\
	{$(INCLUDE)}"OgreHardwareBuffer.h"\
	{$(INCLUDE)}"OgreHardwareBufferManager.h"\
	{$(INCLUDE)}"OgreHardwareIndexBuffer.h"\
	{$(INCLUDE)}"OgreHardwareVertexBuffer.h"\
	{$(INCLUDE)}"OgreImage.h"\
	{$(INCLUDE)}"OgreInput.h"\
	{$(INCLUDE)}"OgreInputEvent.h"\
	{$(INCLUDE)}"OgreIteratorWrappers.h"\
	{$(INCLUDE)}"OgreKeyFrame.h"\
	{$(INCLUDE)}"OgreKeyTarget.h"\
	{$(INCLUDE)}"OgreLight.h"\
	{$(INCLUDE)}"OgreLog.h"\
	{$(INCLUDE)}"OgreLogManager.h"\
	{$(INCLUDE)}"OgreMaterial.h"\
	{$(INCLUDE)}"OgreMaterialManager.h"\
	{$(INCLUDE)}"OgreMaterialSerializer.h"\
	{$(INCLUDE)}"OgreMath.h"\
	{$(INCLUDE)}"OgreMatrix3.h"\
	{$(INCLUDE)}"OgreMatrix4.h"\
	{$(INCLUDE)}"OgreMemoryMacros.h"\
	{$(INCLUDE)}"OgreMemoryManager.h"\
	{$(INCLUDE)}"OgreMesh.h"\
	{$(INCLUDE)}"OgreMeshManager.h"\
	{$(INCLUDE)}"OgreMeshSerializer.h"\
	{$(INCLUDE)}"OgreMeshSerializerImpl.h"\
	{$(INCLUDE)}"OgreMouseEvent.h"\
	{$(INCLUDE)}"OgreMouseMotionTarget.h"\
	{$(INCLUDE)}"OgreMouseTarget.h"\
	{$(INCLUDE)}"OgreMovableObject.h"\
	{$(INCLUDE)}"OgreNode.h"\
	{$(INCLUDE)}"OgreOverlay.h"\
	{$(INCLUDE)}"OgreOverlayManager.h"\
	{$(INCLUDE)}"OgreParticle.h"\
	{$(INCLUDE)}"OgreParticleAffector.h"\
	{$(INCLUDE)}"OgreParticleEmitter.h"\
	{$(INCLUDE)}"OgreParticleEmitterCommands.h"\
	{$(INCLUDE)}"OgreParticleIterator.h"\
	{$(INCLUDE)}"OgreParticleSystem.h"\
	{$(INCLUDE)}"OgreParticleSystemManager.h"\
	{$(INCLUDE)}"OgrePass.h"\
	{$(INCLUDE)}"OgrePatchMesh.h"\
	{$(INCLUDE)}"OgrePatchSurface.h"\
	{$(INCLUDE)}"OgrePlane.h"\
	{$(INCLUDE)}"OgrePlatform.h"\
	{$(INCLUDE)}"OgrePlatformManager.h"\
	{$(INCLUDE)}"OgrePositionTarget.h"\
	{$(INCLUDE)}"OgrePredefinedControllers.h"\
	{$(INCLUDE)}"OgrePrerequisites.h"\
	{$(INCLUDE)}"OgreProfiler.h"\
	{$(INCLUDE)}"OgreProgressiveMesh.h"\
	{$(INCLUDE)}"OgreQuaternion.h"\
	{$(INCLUDE)}"OgreRay.h"\
	{$(INCLUDE)}"OgreRenderable.h"\
	{$(INCLUDE)}"OgreRenderOperation.h"\
	{$(INCLUDE)}"OgreRenderQueue.h"\
	{$(INCLUDE)}"OgreRenderQueueListener.h"\
	{$(INCLUDE)}"OgreRenderSystem.h"\
	{$(INCLUDE)}"OgreRenderSystemCapabilities.h"\
	{$(INCLUDE)}"OgreRenderTarget.h"\
	{$(INCLUDE)}"OgreRenderTargetListener.h"\
	{$(INCLUDE)}"OgreRenderTexture.h"\
	{$(INCLUDE)}"OgreRenderWindow.h"\
	{$(INCLUDE)}"OgreResource.h"\
	{$(INCLUDE)}"OgreResourceManager.h"\
	{$(INCLUDE)}"OgreRoot.h"\
	{$(INCLUDE)}"OgreRotationalSpline.h"\
	{$(INCLUDE)}"OgreSceneManager.h"\
	{$(INCLUDE)}"OgreSceneManagerEnumerator.h"\
	{$(INCLUDE)}"OgreSceneNode.h"\
	{$(INCLUDE)}"OgreSceneQuery.h"\
	{$(INCLUDE)}"OgreScrollEvent.h"\
	{$(INCLUDE)}"OgreSDDataChunk.h"\
	{$(INCLUDE)}"OgreSerializer.h"\
	{$(INCLUDE)}"OgreSharedPtr.h"\
	{$(INCLUDE)}"OgreSimpleRenderable.h"\
	{$(INCLUDE)}"OgreSimpleSpline.h"\
	{$(INCLUDE)}"OgreSingleton.h"\
	{$(INCLUDE)}"OgreSkeleton.h"\
	{$(INCLUDE)}"OgreSkeletonManager.h"\
	{$(INCLUDE)}"OgreSkeletonSerializer.h"\
	{$(INCLUDE)}"OgreSphere.h"\
	{$(INCLUDE)}"OgreStdHeaders.h"\
	{$(INCLUDE)}"OgreString.h"\
	{$(INCLUDE)}"OgreStringConverter.h"\
	{$(INCLUDE)}"OgreStringInterface.h"\
	{$(INCLUDE)}"OgreStringVector.h"\
	{$(INCLUDE)}"OgreSubEntity.h"\
	{$(INCLUDE)}"OgreSubMesh.h"\
	{$(INCLUDE)}"OgreTargetManager.h"\
	{$(INCLUDE)}"OgreTechnique.h"\
	{$(INCLUDE)}"OgreTexture.h"\
	{$(INCLUDE)}"OgreTextureManager.h"\
	{$(INCLUDE)}"OgreTextureUnitState.h"\
	{$(INCLUDE)}"OgreTimer.h"\
	{$(INCLUDE)}"OgreUserDefinedObject.h"\
	{$(INCLUDE)}"OgreVector3.h"\
	{$(INCLUDE)}"OgreVector4.h"\
	{$(INCLUDE)}"OgreVertexBoneAssignment.h"\
	{$(INCLUDE)}"OgreVertexIndexData.h"\
	{$(INCLUDE)}"OgreViewport.h"\
	{$(INCLUDE)}"OS.h"\
	{$(INCLUDE)}"pthread.h"\
	{$(INCLUDE)}"stl\_abbrevs.h"\
	{$(INCLUDE)}"stl\_algobase.c"\
	{$(INCLUDE)}"stl\_algobase.h"\
	{$(INCLUDE)}"stl\_alloc.c"\
	{$(INCLUDE)}"stl\_alloc.h"\
	{$(INCLUDE)}"stl\_alloc_old.h"\
	{$(INCLUDE)}"stl\_bvector.h"\
	{$(INCLUDE)}"stl\_config.h"\
	{$(INCLUDE)}"stl\_config_compat.h"\
	{$(INCLUDE)}"stl\_config_compat_post.h"\
	{$(INCLUDE)}"stl\_construct.h"\
	{$(INCLUDE)}"stl\_cwchar.h"\
	{$(INCLUDE)}"stl\_epilog.h"\
	{$(INCLUDE)}"stl\_function_base.h"\
	{$(INCLUDE)}"stl\_hash_fun.h"\
	{$(INCLUDE)}"stl\_hash_map.h"\
	{$(INCLUDE)}"stl\_hash_set.h"\
	{$(INCLUDE)}"stl\_hashtable.c"\
	{$(INCLUDE)}"stl\_hashtable.h"\
	{$(INCLUDE)}"stl\_iterator.h"\
	{$(INCLUDE)}"stl\_iterator_base.h"\
	{$(INCLUDE)}"stl\_iterator_old.h"\
	{$(INCLUDE)}"stl\_new.h"\
	{$(INCLUDE)}"stl\_pair.h"\
	{$(INCLUDE)}"stl\_prolog.h"\
	{$(INCLUDE)}"stl\_pthread_alloc.c"\
	{$(INCLUDE)}"stl\_pthread_alloc.h"\
	{$(INCLUDE)}"stl\_ptrs_specialize.h"\
	{$(INCLUDE)}"stl\_range_errors.h"\
	{$(INCLUDE)}"stl\_relops_cont.h"\
	{$(INCLUDE)}"stl\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\_site_config.h"\
	{$(INCLUDE)}"stl\_sparc_atomic.h"\
	{$(INCLUDE)}"stl\_threads.c"\
	{$(INCLUDE)}"stl\_threads.h"\
	{$(INCLUDE)}"stl\_uninitialized.h"\
	{$(INCLUDE)}"stl\_vector.c"\
	{$(INCLUDE)}"stl\_vector.h"\
	{$(INCLUDE)}"stl\debug\_debug.c"\
	{$(INCLUDE)}"stl\debug\_debug.h"\
	{$(INCLUDE)}"stl\debug\_hashtable.h"\
	{$(INCLUDE)}"stl\debug\_iterator.h"\
	{$(INCLUDE)}"stl\debug\_relops_cont.h"\
	{$(INCLUDE)}"stl\debug\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\debug\_vector.h"\
	{$(INCLUDE)}"stl\type_traits.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_map.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_set.h"\
	{$(INCLUDE)}"stl\wrappers\_vector.h"\
	{$(INCLUDE)}"stl_user_config.h"\
	
NODEP_CPP_SCENEO=\
	"..\..\..\..\..\..\..\usr\include\pthread.h"\
	
# End Source File
# Begin Source File

SOURCE=..\src\SceneOctreeRenderable.cpp
DEP_CPP_SCENEOC=\
	"..\include\SceneOctreeRenderable.h"\
	{$(INCLUDE)}"config.h"\
	{$(INCLUDE)}"config\_epilog.h"\
	{$(INCLUDE)}"config\_msvc_warnings_off.h"\
	{$(INCLUDE)}"config\_prolog.h"\
	{$(INCLUDE)}"config\stl_apcc.h"\
	{$(INCLUDE)}"config\stl_apple.h"\
	{$(INCLUDE)}"config\stl_as400.h"\
	{$(INCLUDE)}"config\stl_bc.h"\
	{$(INCLUDE)}"config\stl_como.h"\
	{$(INCLUDE)}"config\stl_confix.h"\
	{$(INCLUDE)}"config\stl_cray.h"\
	{$(INCLUDE)}"config\stl_dec.h"\
	{$(INCLUDE)}"config\stl_dec_vms.h"\
	{$(INCLUDE)}"config\stl_dm.h"\
	{$(INCLUDE)}"config\stl_fujitsu.h"\
	{$(INCLUDE)}"config\stl_gcc.h"\
	{$(INCLUDE)}"config\stl_hpacc.h"\
	{$(INCLUDE)}"config\stl_ibm.h"\
	{$(INCLUDE)}"config\stl_icc.h"\
	{$(INCLUDE)}"config\stl_intel.h"\
	{$(INCLUDE)}"config\stl_kai.h"\
	{$(INCLUDE)}"config\stl_msvc.h"\
	{$(INCLUDE)}"config\stl_mwerks.h"\
	{$(INCLUDE)}"config\stl_mycomp.h"\
	{$(INCLUDE)}"config\stl_sco.h"\
	{$(INCLUDE)}"config\stl_select_lib.h"\
	{$(INCLUDE)}"config\stl_sgi.h"\
	{$(INCLUDE)}"config\stl_solaris.h"\
	{$(INCLUDE)}"config\stl_sunpro.h"\
	{$(INCLUDE)}"config\stl_symantec.h"\
	{$(INCLUDE)}"config\stl_watcom.h"\
	{$(INCLUDE)}"config\stl_wince.h"\
	{$(INCLUDE)}"config\stlcomp.h"\
	{$(INCLUDE)}"config\vc_select_lib.h"\
	{$(INCLUDE)}"hash_map"\
	{$(INCLUDE)}"hash_set"\
	{$(INCLUDE)}"OgreAnimationState.h"\
	{$(INCLUDE)}"OgreArchiveEx.h"\
	{$(INCLUDE)}"OgreArchiveManager.h"\
	{$(INCLUDE)}"OgreAutoParamDataSource.h"\
	{$(INCLUDE)}"OgreAxisAlignedBox.h"\
	{$(INCLUDE)}"OgreBlendMode.h"\
	{$(INCLUDE)}"OgreCamera.h"\
	{$(INCLUDE)}"OgreColourValue.h"\
	{$(INCLUDE)}"OgreCommon.h"\
	{$(INCLUDE)}"OgreConfig.h"\
	{$(INCLUDE)}"OgreController.h"\
	{$(INCLUDE)}"OgreDataChunk.h"\
	{$(INCLUDE)}"OgreDynLibManager.h"\
	{$(INCLUDE)}"OgreHardwareBuffer.h"\
	{$(INCLUDE)}"OgreHardwareBufferManager.h"\
	{$(INCLUDE)}"OgreHardwareIndexBuffer.h"\
	{$(INCLUDE)}"OgreHardwareVertexBuffer.h"\
	{$(INCLUDE)}"OgreImage.h"\
	{$(INCLUDE)}"OgreIteratorWrappers.h"\
	{$(INCLUDE)}"OgreLight.h"\
	{$(INCLUDE)}"OgreMaterial.h"\
	{$(INCLUDE)}"OgreMath.h"\
	{$(INCLUDE)}"OgreMatrix3.h"\
	{$(INCLUDE)}"OgreMatrix4.h"\
	{$(INCLUDE)}"OgreMemoryMacros.h"\
	{$(INCLUDE)}"OgreMemoryManager.h"\
	{$(INCLUDE)}"OgreMesh.h"\
	{$(INCLUDE)}"OgreMeshManager.h"\
	{$(INCLUDE)}"OgreMovableObject.h"\
	{$(INCLUDE)}"OgreNode.h"\
	{$(INCLUDE)}"OgrePatchMesh.h"\
	{$(INCLUDE)}"OgrePatchSurface.h"\
	{$(INCLUDE)}"OgrePlane.h"\
	{$(INCLUDE)}"OgrePlatform.h"\
	{$(INCLUDE)}"OgrePlatformManager.h"\
	{$(INCLUDE)}"OgrePrerequisites.h"\
	{$(INCLUDE)}"OgreProgressiveMesh.h"\
	{$(INCLUDE)}"OgreQuaternion.h"\
	{$(INCLUDE)}"OgreRay.h"\
	{$(INCLUDE)}"OgreRenderable.h"\
	{$(INCLUDE)}"OgreRenderOperation.h"\
	{$(INCLUDE)}"OgreRenderQueue.h"\
	{$(INCLUDE)}"OgreResource.h"\
	{$(INCLUDE)}"OgreResourceManager.h"\
	{$(INCLUDE)}"OgreRoot.h"\
	{$(INCLUDE)}"OgreSceneManager.h"\
	{$(INCLUDE)}"OgreSceneManagerEnumerator.h"\
	{$(INCLUDE)}"OgreSceneNode.h"\
	{$(INCLUDE)}"OgreSceneQuery.h"\
	{$(INCLUDE)}"OgreSharedPtr.h"\
	{$(INCLUDE)}"OgreSingleton.h"\
	{$(INCLUDE)}"OgreSphere.h"\
	{$(INCLUDE)}"OgreStdHeaders.h"\
	{$(INCLUDE)}"OgreString.h"\
	{$(INCLUDE)}"OgreTexture.h"\
	{$(INCLUDE)}"OgreTextureManager.h"\
	{$(INCLUDE)}"OgreVector3.h"\
	{$(INCLUDE)}"OgreVector4.h"\
	{$(INCLUDE)}"OgreVertexBoneAssignment.h"\
	{$(INCLUDE)}"OgreVertexIndexData.h"\
	{$(INCLUDE)}"OS.h"\
	{$(INCLUDE)}"pthread.h"\
	{$(INCLUDE)}"stl\_abbrevs.h"\
	{$(INCLUDE)}"stl\_algobase.c"\
	{$(INCLUDE)}"stl\_algobase.h"\
	{$(INCLUDE)}"stl\_alloc.c"\
	{$(INCLUDE)}"stl\_alloc.h"\
	{$(INCLUDE)}"stl\_alloc_old.h"\
	{$(INCLUDE)}"stl\_bvector.h"\
	{$(INCLUDE)}"stl\_config.h"\
	{$(INCLUDE)}"stl\_config_compat.h"\
	{$(INCLUDE)}"stl\_config_compat_post.h"\
	{$(INCLUDE)}"stl\_construct.h"\
	{$(INCLUDE)}"stl\_cwchar.h"\
	{$(INCLUDE)}"stl\_epilog.h"\
	{$(INCLUDE)}"stl\_function_base.h"\
	{$(INCLUDE)}"stl\_hash_fun.h"\
	{$(INCLUDE)}"stl\_hash_map.h"\
	{$(INCLUDE)}"stl\_hash_set.h"\
	{$(INCLUDE)}"stl\_hashtable.c"\
	{$(INCLUDE)}"stl\_hashtable.h"\
	{$(INCLUDE)}"stl\_iterator.h"\
	{$(INCLUDE)}"stl\_iterator_base.h"\
	{$(INCLUDE)}"stl\_iterator_old.h"\
	{$(INCLUDE)}"stl\_new.h"\
	{$(INCLUDE)}"stl\_pair.h"\
	{$(INCLUDE)}"stl\_prolog.h"\
	{$(INCLUDE)}"stl\_pthread_alloc.c"\
	{$(INCLUDE)}"stl\_pthread_alloc.h"\
	{$(INCLUDE)}"stl\_ptrs_specialize.h"\
	{$(INCLUDE)}"stl\_range_errors.h"\
	{$(INCLUDE)}"stl\_relops_cont.h"\
	{$(INCLUDE)}"stl\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\_site_config.h"\
	{$(INCLUDE)}"stl\_sparc_atomic.h"\
	{$(INCLUDE)}"stl\_threads.c"\
	{$(INCLUDE)}"stl\_threads.h"\
	{$(INCLUDE)}"stl\_uninitialized.h"\
	{$(INCLUDE)}"stl\_vector.c"\
	{$(INCLUDE)}"stl\_vector.h"\
	{$(INCLUDE)}"stl\debug\_debug.c"\
	{$(INCLUDE)}"stl\debug\_debug.h"\
	{$(INCLUDE)}"stl\debug\_hashtable.h"\
	{$(INCLUDE)}"stl\debug\_iterator.h"\
	{$(INCLUDE)}"stl\debug\_relops_cont.h"\
	{$(INCLUDE)}"stl\debug\_relops_hash_cont.h"\
	{$(INCLUDE)}"stl\debug\_vector.h"\
	{$(INCLUDE)}"stl\type_traits.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_map.h"\
	{$(INCLUDE)}"stl\wrappers\_hash_set.h"\
	{$(INCLUDE)}"stl\wrappers\_vector.h"\
	{$(INCLUDE)}"stl_user_config.h"\
	
NODEP_CPP_SCENEOC=\
	"..\..\..\..\..\..\..\usr\include\pthread.h"\
	
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\include\DotSceneManager.h
# End Source File
# Begin Source File

SOURCE=..\include\SceneMain.h
# End Source File
# Begin Source File

SOURCE=..\include\SceneOctree.h
# End Source File
# Begin Source File

SOURCE=..\include\SceneOctreeRenderable.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Group "TinyXML"

# PROP Default_Filter ""
# Begin Source File

SOURCE=..\..\..\..\..\ogrenew\Tools\XMLConverter\src\tinystr.cpp
DEP_CPP_TINYS=\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\ogrenew\Tools\XMLConverter\src\tinyxml.cpp
DEP_CPP_TINYX=\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\ogrenew\Tools\XMLConverter\src\tinyxmlerror.cpp
DEP_CPP_TINYXM=\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h"\
	
# End Source File
# Begin Source File

SOURCE=..\..\..\..\..\ogrenew\Tools\XMLConverter\src\tinyxmlparser.cpp
DEP_CPP_TINYXML=\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinystr.h"\
	"..\..\..\..\..\ogrenew\Tools\XMLConverter\include\tinyxml.h"\
	
# End Source File
# End Group
# End Target
# End Project
