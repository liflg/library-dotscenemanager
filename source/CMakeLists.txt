cmake_minimum_required(VERSION 2.8)
project(Plugin_DotSceneManager)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/include/Attic/
)

add_definitions(-DTIXML_USE_STL)

set(DotSceneManager_SOURCES
    src/Attic/dotSceneUtils.cpp
    src/Attic/SceneOctree.cpp
    src/Attic/SceneOctreeRenderable.cpp
    src/Attic/SceneMain.cpp
    src/Attic/DotSceneManager.cpp
)

set(DotSceneManager_HEADER
    include/Attic/SceneMain.h
    include/Attic/dotSceneUtils.h
    include/Attic/SceneOctreeRenderable.h
    include/Attic/SceneOctree.h
    include/Attic/DotSceneManager.h
)

add_library(Plugin_DotSceneManager SHARED
    ${DotSceneManager_SOURCES}
    ${DotSceneManager_HEADER}
)

install(TARGETS Plugin_DotSceneManager LIBRARY DESTINATION lib)
install(FILES ${DotSceneManager_HEADER} DESTINATION include)

