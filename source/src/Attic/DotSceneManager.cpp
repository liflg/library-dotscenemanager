////////////////////////////////////////////////////////////////////////////////
// DotSceneOctreeManager.h
// Author     : Doug Wolanick
// Start Date : May 27, 2003
// Copyright  : (C) 2003 by Doug Wolanick
// Email      : Doug@IceTecStudios.com
////////////////////////////////////////////////////////////////////////////////

/***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU Lesser General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************/

#include "DotSceneManager.h"
#include "OgreImage.h"
#include "OgreConfigFile.h"
#include "OgreMaterial.h"
#include "OgreCamera.h"
#include "OgreException.h"
#include "OgreStringConverter.h"
#include "OgreRenderSystem.h"
#include "OgreEntity.h"
#include "OgreSubEntity.h"
#include "OgreSceneNode.h"
#include "OgreMovableObject.h"
#include <fstream>
#include <vector>
#include <OgreLogManager.h>

using namespace Ogre;
using namespace std;

DotSceneOctreeManager::DotSceneOctreeManager(const String& name) : 	SceneManager(name)
{
	OctreeVisible = 0;
	mSceneMain = NULL;
}

DotSceneOctreeManager::~DotSceneOctreeManager()
{
}

const String& DotSceneOctreeManager::getTypeName(void) const
{
	return DotSceneOctreeManagerFactory::FACTORY_TYPE_NAME;
}

void DotSceneOctreeManager::setWorldGeometry( const String& filename )
{
	addWorldGeometry( filename, ResourceGroupManager::getSingleton().getWorldResourceGroupName() );
}

void DotSceneOctreeManager::addWorldGeometry( const String& filename, const String& groupname )
{
	mSceneMain = new DotSceneMain();

	mSceneMain->setSceneManager( this );
	mSceneMain->load( filename, groupname );

	LoadedScenes.push_back( mSceneMain );
}

void DotSceneOctreeManager::addWorldGeometry( const String& filename , const String& name, float xOffset, float zOffset, const String& groupname )
{
	mSceneMain = new DotSceneMain();

	mSceneMain->setSceneManager( this );
	mSceneMain->setPos( xOffset, 0 , zOffset );
	mSceneMain->setName( name );

	mSceneMain->load( filename, groupname );

	LoadedScenes.push_back( mSceneMain );
}

void DotSceneOctreeManager::removeWorldGeometry( const String &name )
{
	vector <DotSceneMain*>::iterator CurScene;

	for( CurScene = LoadedScenes.begin(); CurScene != LoadedScenes.end(); ++CurScene )
	{
		if( (*CurScene)->getName() == name )
		{
			(*CurScene)->unload();
			delete( *CurScene );

			CurScene = LoadedScenes.erase( CurScene );
		}
	}
}

void DotSceneOctreeManager::removeAllWorldGeometry( void )
{
	vector <DotSceneMain*>::iterator CurScene;

	for( CurScene = LoadedScenes.begin(); CurScene != LoadedScenes.end(); ++CurScene )
	{
		(*CurScene)->unload();
		delete( *CurScene );
	}

	LoadedScenes.clear();
}

void DotSceneOctreeManager::clearScene( void )
{
	removeAllWorldGeometry();
	SceneManager::clearScene();
}

void DotSceneOctreeManager::setOctreeVisible( bool Visible )
{
	if( Visible != OctreeVisible )
	{
		OctreeVisible = Visible;

		vector<DotSceneMain*>::iterator CurScene;
		for( CurScene=LoadedScenes.begin(); CurScene!=LoadedScenes.end(); ++CurScene )
			(*CurScene)->setOctreeVisible( Visible );
	}
}

void DotSceneOctreeManager::_updateSceneGraph( Camera * cam )
{
	SceneManager::_updateSceneGraph( cam );
}

void DotSceneOctreeManager::_renderVisibleObjects( void )
{
	SceneManager::_renderVisibleObjects();

	DotSceneOctreeRenderable::mRenderedTris = 0;
}

/*void DotSceneOctreeManager::_findVisibleObjects ( Camera * cam, VisibleObjectsBoundsInfo* visibleBounds, bool onlyShadowCasters )
{
	mRenderQueue->clear();
	SceneManager::_findVisibleObjects( cam, visibleBounds, onlyShadowCasters );
*/
	// Uncomment this to do no frustum culling - For Debug Purposes
	/*
	vector <DotSceneMain*>::iterator CurScene;

	for( CurScene = LoadedScenes.begin(); CurScene != LoadedScenes.end(); ++CurScene )
	{
	std::vector <DotSceneOctreeRenderable*> RenderableList;
	std::vector <DotSceneOctreeRenderable*>::iterator CurRenderable;

	RenderableList = (*CurScene)->getAllRenderables();


	for( CurRenderable=RenderableList.begin(); CurRenderable!=RenderableList.end(); ++CurRenderable )
	{
	mRenderQueue.addRenderable( *CurRenderable );
	}
	}
	*/

//}
bool DotSceneOctreeManager::setOption( const String& strKey, const void* pValue )
{
	if (strKey == "OctreeVisible")
	{
		const bool *v = static_cast < const bool * >(pValue);
		setOctreeVisible( *v );
		return true;
	}
	if (strKey == "removeAllWorldGeometry")
	{
		removeAllWorldGeometry();
		return true;
	}
	if (strKey == "removeWorldGeometry")
	{
		const String *v = static_cast < const String * >(pValue);
		removeWorldGeometry( *v );
		return true;
	}
	if (strKey == "addWorldGeometry")
	{
		const StringVector &v = *(static_cast < const StringVector * >(pValue));
		if (v.size() == 2)
		{
			String filename = v[0];
			String groupname = v[1];
			addWorldGeometry( filename, groupname );
			return true;
		}
		if (v.size() == 5)
		{
			String filename = v[0];
			String name = v[1];
			float xoffset = (float)StringConverter::parseReal(v[2]);
			float yoffset = (float)StringConverter::parseReal(v[3]);
			String groupname = v[4];
			addWorldGeometry( filename, name, xoffset, yoffset, groupname );
			return true;
		}
	}
	return false;
}

bool DotSceneOctreeManager::getOption( const String& strKey, void* pDestValue )
{
	if (strKey == "hasWorldGeometry") 
	{ 

		bool *v = static_cast < bool * >(pDestValue); 
		if (!mSceneMain) return *v = false;
		else
			*v = (mSceneMain->getNumMeshes(NULL)>0);
		return true;
	} 
	if (strKey == "NumMeshes") 
	{ 
		int *v = static_cast < int * >(pDestValue); 
		*v = getNumMeshes(0); 
		return true; 
	} 
	if (strKey == "OctreeVisible") 
	{ 
		bool *v = static_cast < bool * >(pDestValue); 
		*v = OctreeVisible; 
		return true; 
	} 
	if (StringUtil::startsWith(strKey, "VerticesPtr", false)) 
	{ 
		int idx = StringConverter::parseInt( strKey.substr( 11) ); 
		float **v = static_cast < float ** >(pDestValue); 
		*v = getVerticesPtr("", idx); 
		return true; 
	} 
	if (StringUtil::startsWith(strKey, "IndexPtr", false)) 
	{ 
		int idx = StringConverter::parseInt( strKey.substr( 8 ) ); 
		int **v = static_cast < int ** >(pDestValue); 
		*v = getIndexPtr("", idx); 
		return true; 
	} 
	if (StringUtil::startsWith(strKey, "NumVertices", false)) 
	{ 
		int idx = StringConverter::parseInt( strKey.substr( 11 ) ); 
		int *v = static_cast < int * >(pDestValue); 
		*v = getNumVertices("", idx); 
		return true; 
	} 
	if (StringUtil::startsWith(strKey, "NumIndex", false)) 
	{ 
		int idx = StringConverter::parseInt( strKey.substr( 8 ) ); 
		int *v = static_cast < int * >(pDestValue); 
		*v = getNumIndex("", idx); 
		return true; 
	} 
	if (StringUtil::startsWith(strKey, "BoundingBox", false)) 
	{ 
		int idx = StringConverter::parseInt( strKey.substr( 11 ) ); 
		AxisAlignedBox *v = static_cast < AxisAlignedBox * >(pDestValue); 
		*v = getBoundingBox("", idx); 
		return true; 
	}
	int *v = static_cast < int * >(pDestValue);
	*v = 0;

	return false; 
} 

bool DotSceneOctreeManager::hasOption( const String& strKey ) const 
{ 
	if (strKey == "hasWorldGeometry") 
		return true;

	if (strKey == "NumMeshes") 
	{ 
		return true; 
	} 
	if (strKey == "OctreeVisible") 
	{ 
		return true; 
	} 

	int numMeshes = mSceneMain->getNumMeshes( 0 ); 
	int idx=-1; 
	if (StringUtil::startsWith(strKey, "VerticesPtr", false)) 
		idx = StringConverter::parseInt( strKey.substr( 11) ); 
	else if (StringUtil::startsWith(strKey, "IndexPtr", false)) 
		idx = StringConverter::parseInt( strKey.substr( 8 ) ); 
	else if (StringUtil::startsWith(strKey, "NumVertices", false)) 
		idx = StringConverter::parseInt( strKey.substr( 11 ) ); 
	else if (StringUtil::startsWith(strKey, "NumIndex", false)) 
		idx = StringConverter::parseInt( strKey.substr( 8 ) ); 
	else if (StringUtil::startsWith(strKey, "BoundingBox", false)) 
		idx = StringConverter::parseInt( strKey.substr( 11 ) ); 

	if ((idx>=0) && (idx<numMeshes)) 
		return true; 
	return false; 
} 


bool DotSceneOctreeManager::getOptionValues( const String& strKey, StringVector& refValueList)
{
	//return SceneManager::getOptionValues( strKey, refValueList );
	return false; //TODO: implement this.
}

bool DotSceneOctreeManager::getOptionKeys( StringVector& refKeys )
{
	refKeys.clear();
	refKeys.push_back("NumMeshes");
	refKeys.push_back("VerticesPtr#");
	refKeys.push_back("IndexPtr#");
	refKeys.push_back("NumVertices#");
	refKeys.push_back("NumIndex#");
	refKeys.push_back("BoundingBox#");
	refKeys.push_back("OctreeVisible");
	refKeys.push_back("removeAllWorldGeometry");
	refKeys.push_back("removeWorldGeometry");
	refKeys.push_back("addWorldGeometry");

	return true;
}

const String DotSceneOctreeManagerFactory::FACTORY_TYPE_NAME = "DotSceneOctreeManager";
//-----------------------------------------------------------------------
void DotSceneOctreeManagerFactory::initMetaData(void) const
{
	mMetaData.typeName = FACTORY_TYPE_NAME;
	mMetaData.description = "Scene manager organising the scene on the basis of an octree.";
	mMetaData.sceneTypeMask = 0xFFFF; // support all types
	mMetaData.worldGeometrySupported = false;
}
//-----------------------------------------------------------------------
SceneManager* DotSceneOctreeManagerFactory::createInstance(
	const String& instanceName)
{
	return new DotSceneOctreeManager(instanceName);
}
//-----------------------------------------------------------------------
void DotSceneOctreeManagerFactory::destroyInstance(SceneManager* instance)
{
	delete instance;
}

//-----------------------------------------------------------------------

IntersectionSceneQuery* DotSceneOctreeManager::createIntersectionQuery(unsigned long mask)
{
	DotSceneOctreeIntersectionSceneQuery* q = new DotSceneOctreeIntersectionSceneQuery(this);
	q->setQueryMask(mask);
	return q;
}

DotSceneOctreeIntersectionSceneQuery::DotSceneOctreeIntersectionSceneQuery(SceneManager* creator)
: DefaultIntersectionSceneQuery(creator)
{
	// Add bounds fragment type
	mSupportedWorldFragments.insert(SceneQuery::WFT_CUSTOM_GEOMETRY);
}

void DotSceneOctreeIntersectionSceneQuery::execute(IntersectionSceneQueryListener* listener)
{
	// Do movables to movables
	DefaultIntersectionSceneQuery::execute(listener);

	// Do entities to world
	std::vector <DotSceneOctreeRenderable*> RenderableArray = ((DotSceneOctreeManager*)mParentSceneMgr)->mSceneMain->getAllRenderables ();
	vector <DotSceneOctreeRenderable*>::iterator CurRenderable;
	SceneQuery::WorldFragment fragment;

	Root::MovableObjectFactoryIterator factIt =
		Root::getSingleton().getMovableObjectFactoryIterator();
	while(factIt.hasMoreElements())
	{
		SceneManager::MovableObjectIterator objItA =
			mParentSceneMgr->getMovableObjectIterator(
			factIt.getNext()->getType());
		while (objItA.hasMoreElements())
		{
			MovableObject* a = objItA.getNext();
			// Apply mask
			if ( a->getQueryFlags() & mQueryMask)
			{
				const AxisAlignedBox& box1 = a->getWorldBoundingBox();

				for( CurRenderable = RenderableArray.begin(); CurRenderable!=RenderableArray.end(); ++CurRenderable )
				{
					const AxisAlignedBox& box2 = (*CurRenderable)->getBoundingBox ();
					if (box1.intersects(box2))
					{
						fragment.fragmentType = SceneQuery::WFT_CUSTOM_GEOMETRY;
						fragment.geometry = (*CurRenderable);

						listener->queryResult(a, &fragment);
					}
				}
			}
		}
	}
}
