/*
-----------------------------------------------------------------------------
This source file is part of OGRE
(Object-oriented Graphics Rendering Engine)
For the latest info, see http://ogre.sourceforge.net/

Copyright � 2003 Doug Wolanick

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

#include "SceneMain.h"
#include "dotSceneUtils.h"
#include "tinyxml.h"

using namespace Ogre;
using namespace Ogre::dsi;

DotSceneMain::DotSceneMain()
{
	mSceneManager = 0;
	RootNode = 0;

	OctreeVisible = 0;
	Octree = 0;
	Position = Vector3::ZERO;
	//Terrain = 0;
}

DotSceneMain::~DotSceneMain()
{	
}

void DotSceneMain::setOctreeVisible( bool Visible )
{
	OctreeVisible = Visible;
	Octree->setOctreeVisible( Visible );
}

void DotSceneMain::setPos( Vector3 NewPos )
{
	Position = NewPos;

	if( RootNode ){
		RootNode->setPosition( Position );
	}
}

void DotSceneMain::setPos( float x, float y, float z )
{
	Position.x = x;
	Position.y = y;
	Position.z = z;

	if( RootNode ){
		RootNode->setPosition( Position );
	}
}

void DotSceneMain::setSceneManager( Ogre::SceneManager* NewSceneManager ){
	mSceneManager = NewSceneManager;	
}

int DotSceneMain::load(  const Ogre::String& Filename, const Ogre::String& groupname  )
{
	// Create the Root Scene Node for this scene
	RootNode = mSceneManager->getRootSceneNode()->createChildSceneNode();
	RootNode->setPosition( Position );

	TiXmlDocument	*XMLDoc;
	TiXmlElement	*XMLRoot, *XMLElement;

	// Look for the file in the local folder first
	DataStreamPtr pStream = ResourceGroupManager::getSingleton().openResource( Filename, groupname );
	String data = pStream->getAsString();
	XMLDoc = new TiXmlDocument();
	XMLDoc->Parse( data.c_str() );
	pStream->close();
	pStream.setNull();

	if( XMLDoc->Error() ) {
		delete XMLDoc;
		OGRE_EXCEPT( Exception::ERR_INVALIDPARAMS,
			"Error: Parsing .scene file failed!!!: " + Filename, "DotSceneMain::load" );
	}

	// Validate the File
	XMLRoot = XMLDoc->RootElement();
	if( String(XMLRoot->Value()) != "scene" ) {
		delete XMLDoc;
		OGRE_EXCEPT( Exception::ERR_INVALIDPARAMS, 
			"Error: Invalid .scene File. Missing <scene> : " + Filename, "DotSceneMain::load" );
	}

	// load Environment Settings Such as Fog and Ambient Light
	XMLElement = XMLRoot->FirstChildElement("environment");

	if( XMLElement )
		loadEnvironment( XMLElement );

	// If There is an Octree
	XMLElement = XMLRoot->FirstChildElement("octree");
	if( XMLElement ){
		// load the Octree
		Octree = new DotSceneOctree();
		Octree->setSceneManager( mSceneManager );
		Octree->setRootNode( RootNode );
		Octree->load( XMLElement, groupname );
	}

	XMLElement = XMLRoot->FirstChildElement("nodes");
	if (XMLElement) XMLElement = XMLElement->FirstChildElement("node");
	while (XMLElement)
	{
		// only do lights
		if (!XMLElement->FirstChildElement("light"))
		{
			XMLElement = XMLElement->NextSiblingElement("node");
			continue;
		}

		// name
		String name = String("_mln_") + XMLElement->Attribute("name");

		// pos
		TiXmlElement *ele = XMLElement->FirstChildElement("position");
		Ogre::Vector3 pos;
		if (ele) pos = xml::readPosition(ele); else pos = Ogre::Vector3::ZERO;

		// rotation
		ele = XMLElement->FirstChildElement("rotation");
		Ogre::Quaternion rot;
		if (ele) rot = xml::readOrientation(ele); else rot = Ogre::Quaternion::IDENTITY;

		Ogre::SceneNode *node = RootNode->createChildSceneNode(name, pos, rot);

		TiXmlElement *lightele = XMLElement->FirstChildElement("light");
		name = lightele->Attribute("name");

		//
		Light *pLight = mSceneManager->createLight(String("_ml_") + name);
		node->attachObject(pLight);

		TiXmlElement *pElem = NULL;
		// visibility
		bool visible = true;
		if (XMLElement->Attribute("visible"))
			if (String(XMLElement->Attribute("visible")) == String("false"))
				visible = false;
		pLight->setVisible(visible);
		// colours
		TiXmlElement *difElem = lightele->FirstChildElement("colourDiffuse");
		TiXmlElement *speElem = lightele->FirstChildElement("colourSpecular");
		if (difElem)
			pLight->setDiffuseColour(xml::readColor(difElem));
		if (speElem)
			pLight->setSpecularColour(xml::readColor(speElem));
		// type
		String type = lightele->Attribute("type") ? lightele->Attribute("type") : "point";
		if (type == "point")
			pLight->setType(Light::LT_POINT);
		else if (type == "directional" || type == "targetDirectional")
			pLight->setType(Light::LT_DIRECTIONAL);
		else if (type == "spot" || type == "targetSpot")
			pLight->setType(Light::LT_SPOTLIGHT);
		// attenuation
		pElem = lightele->FirstChildElement("lightAttenuation");
		if (pElem)
		{
			Real range, constant, linear, quadratic;
			range = StringConverter::parseReal(pElem->Attribute("range"));
			constant = StringConverter::parseReal(pElem->Attribute("constant"));
			linear = StringConverter::parseReal(pElem->Attribute("linear"));
			quadratic = StringConverter::parseReal(pElem->Attribute("quadratic"));
			pLight->setAttenuation(range, constant, linear, quadratic);
		}
		// spotlight range
		pElem = lightele->FirstChildElement("lightRange");
		if (pElem)
		{
			Real inner, outer, falloff;
			inner = StringConverter::parseReal(pElem->Attribute("inner"));
			outer = StringConverter::parseReal(pElem->Attribute("outer"));
			falloff = StringConverter::parseReal(pElem->Attribute("falloff"));
			pLight->setSpotlightRange(Ogre::Angle(inner), Ogre::Angle(outer), falloff);
		}
		// direction
		TiXmlElement *nElem = lightele->FirstChildElement("normal");
		if (nElem)
			pLight->setDirection(xml::readNormal(nElem));

		XMLElement = XMLElement->NextSiblingElement("node");
	}


	// If There is a Terrain
	XMLElement = XMLRoot->FirstChildElement("terrain");
	if( XMLElement ){
		// load the Terrain
		//Terrain = new SceneTerrain();
		//Terrain->setSceneManager( mSceneManager );
		//Terrain->load( XMLElement );
	}

	delete XMLDoc;
	return 1;
}

void DotSceneMain::setName( String NewName )
{
	MyName = NewName;
}

String DotSceneMain::getName( void )
{
	return MyName;
}

void DotSceneMain::unload()
{
	if( Octree ){
		Octree->unload();
		delete Octree;

		//Terrain->unload();
	}
}

void DotSceneMain::setTerrainGeometry( Ogre::String Filename )
{
	//Terrain->SetWorldGeometry( Filename );
}

void DotSceneMain::loadEnvironment( TiXmlElement *Element )
{
	TiXmlElement *EnvElement;

	// load Ambient Lighting
	EnvElement = Element->FirstChildElement("colourAmbient");
	if( EnvElement ){
		Ambient.r = StringConverter::parseReal( EnvElement->Attribute("r") );
		Ambient.g = StringConverter::parseReal( EnvElement->Attribute("g") );
		Ambient.b = StringConverter::parseReal( EnvElement->Attribute("b") );

		// Make the Changes to the SceneManager
		mSceneManager->setAmbientLight( Ambient );
	}

	// load Fog
	EnvElement = Element->FirstChildElement("fog");
	if( EnvElement ){
		// Type
		String FogType = EnvElement->Attribute("mode");
		if(      stricmp( FogType.c_str() , "linear" ) == 0 ) Fog.Type = FOG_LINEAR;
		else if( stricmp( FogType.c_str() , "exp"    ) == 0 ) Fog.Type = FOG_EXP;
		else if( stricmp( FogType.c_str() , "exp2"   ) == 0 ) Fog.Type = FOG_EXP2;
		else Fog.Type = FOG_NONE;

		// Color
		TiXmlElement* XMLColour = EnvElement->FirstChildElement( "colourDiffuse" );
		if( XMLColour ) {
			Fog.Color.r = StringConverter::parseReal( XMLColour->Attribute("r") );
			Fog.Color.g = StringConverter::parseReal( XMLColour->Attribute("g") );
			Fog.Color.b = StringConverter::parseReal( XMLColour->Attribute("b") );
		}
		else
			Fog.Color.r = Fog.Color.g = Fog.Color.b = 255;	//Else set to white

		// Density, Start, and End
		Fog.Density = StringConverter::parseReal( EnvElement->Attribute("expDensity") );
		Fog.Start   = StringConverter::parseReal( EnvElement->Attribute("linearStart") );
		Fog.End     = StringConverter::parseReal( EnvElement->Attribute("linearEnd") );

		// Make the Changes to the SceneManager
		mSceneManager->setFog( Fog.Type , Fog.Color, Fog.Density, Fog.Start, Fog.End );
	}
}

// Parameter info:
// MaterialName = Ogre .material name such as 'Examples/Water'
// Filename = Texture file such as 'water.jpg'
void DotSceneMain::setTerrainTexture( Ogre::String MaterialName, String Filename )
{
	if( !Filename.empty() ){
		//Terrain->SetMaterialFile( Filename );
	}
	else{
		//Terrain->SetMaterialName( MaterialName );
	}

}

int DotSceneMain::getNumMeshes( char* name )
{
	int Result=0;

	if( name == 0 )
	{
		// Just check the first Octree
		if( Octree )
		{
			Result = Octree->getTotalMeshes();
		}
	}

	return Result;
}

float* DotSceneMain::getVerticesPtr( char* name, int MeshIndex )
{
	float* Result = 0;

	Result = Octree->getVerticesPtr( MeshIndex );

	return Result;
}

int* DotSceneMain::getIndexPtr( char* name, int MeshIndex )
{
	int* Result = 0;

	Result = Octree->getIndexPtr( MeshIndex );

	return Result;
}

int DotSceneMain::getNumVertices( char* name, int MeshIndex )
{
	int Result = 0;

	Result = Octree->getNumVertices( MeshIndex );

	return Result;
}

int DotSceneMain::getNumIndex( char* name, int MeshIndex )
{
	int Result = 0;

	Result = Octree->getNumIndex( MeshIndex );

	return Result;
}

std::vector <DotSceneOctreeRenderable*> DotSceneMain::getAllRenderables()
{
	return Octree->getAllRenderables();
}

const AxisAlignedBox& DotSceneMain::getBoundingBox( char* name, int MeshIndex )
{
	return Octree->getBoundingBox( MeshIndex );
}
