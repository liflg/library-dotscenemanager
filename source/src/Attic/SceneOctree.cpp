/*
-----------------------------------------------------------------------------
This source file is part of OGRE
(Object-oriented Graphics Rendering Engine)
For the latest info, see http://ogre.sourceforge.net/

Copyright � 2003 Doug Wolanick

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA, or go to
http://www.gnu.org/copyleft/lesser.txt.
-----------------------------------------------------------------------------
*/

#include "SceneOctree.h"
#include "OgreResourceGroupManager.h"
#include <sstream>

using namespace Ogre;
using namespace std;


///////////////////////////////////////////////////////////////////////////////////////////////
//// DotSceneOctree //////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

DotSceneOctree::DotSceneOctree()
{
	RootNode = 0;
	TotalOctreeMeshes = 0;
}

DotSceneOctree::~DotSceneOctree()
{
}

void DotSceneOctree::setOctreeVisible( bool Visible )
{
	OctreeVisible = Visible;

	vector<SceneNode*>::iterator CurNode;
	for( CurNode = SceneNodeArray.begin(); CurNode!=SceneNodeArray.end(); ++CurNode )
		(*CurNode)->showBoundingBox( Visible );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Function: load
//	Input:
//		TiXmlElement *Element - The XML element passed in must be the <octree> tag from a .scene file
//    Ogre::String &_path - path of .scene file
//	Summary:
int DotSceneOctree::load( TiXmlElement *Element, const String& groupname )
{
	if( String(Element->Value()) != "octree" )
	{
		LogManager::getSingleton().logMessage(LML_CRITICAL, "ERROR: DotScene plugin DotSceneOctree::load found no 'octree' xml info *****");
		return 0;
	}

	LogManager::getSingleton().logMessage("DotSceneOctreeManager: Reading Octree Data...");

	// Find the name of the binary file
	const char *BinFile = Element->Attribute("binFile");
	if (!BinFile)
	{
		LogManager::getSingleton().logMessage(LML_CRITICAL, "ERROR: DotScene plugin DotSceneOctree::load found no 'binFile' xml info *****");
		return 0;
	}
	// Open the binary file
	mStream = ResourceGroupManager::getSingleton().openResource( BinFile, groupname );

	// load the Shared Geometry
	TiXmlElement* XMLSharedGeometry = Element->FirstChildElement("octsharedgeometry");
	if( XMLSharedGeometry )
		loadSharedGeometry( XMLSharedGeometry );

	// Recursively load the Octree Nodes
	TiXmlElement* SubElement = Element->FirstChildElement("octNode");
	while( SubElement )
	{
		readNode( RootNode, SubElement );
		SubElement = SubElement->NextSiblingElement("octNode");
	}

	RootNode->_update( true, false );

	// Done loading Octree			
	mStream->close();
	mStream.setNull();
	return 1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
// Function: loadSharedGeometry
//	Input:
//		TiXmlElement *XMLSharedGeometry	-	Pointer to the XML element that defines the shared geometry
//		conforming to the .scene format
//	Summary:
//		To speed up compiling and reduce the number of state changes required when rendering, all of the
//	vertex data in the scene is grouped by material, then stored into a shared buffer. 1 buffer per material.
//	This function loads the shared geometry as defined in the .scene file into the VertexBuffers.
void DotSceneOctree::loadSharedGeometry( TiXmlElement *XMLSharedGeometry )
{
	TiXmlElement* XMLOctGeometry = XMLSharedGeometry->FirstChildElement("octGeometry");

	// All vertices of the same material in the scene are shared. This is
	// where we load the information into a shared buffer.
	while( XMLOctGeometry )
	{
		int BinaryDataOffset=0, VertTotal=0, NormalTotal=0, ColorTotal=0, TexSets=0, TangentTotal=0;
		String MaterialName;

		// load the Attributes of this Geometry Chunk
		if( XMLOctGeometry->Attribute("binaryDataOffset") )
			BinaryDataOffset = StringConverter::parseInt(String(XMLOctGeometry->Attribute("binaryDataOffset")));
		else
		{
			LogManager::getSingleton().logMessage(LML_CRITICAL, "ERROR: DotScene plugin DotSceneOctree::load found no 'binaryDataOffset' xml info *****");
			XMLOctGeometry = XMLOctGeometry->NextSiblingElement("octGeometry");
			continue;
		}

		if( XMLOctGeometry->Attribute("materialName") )
			MaterialName = XMLOctGeometry->Attribute("materialName");
		else
		{
			LogManager::getSingleton().logMessage(LML_CRITICAL, "ERROR: DotScene plugin DotSceneOctree::load found no 'materialName' xml info *****");
			XMLOctGeometry = XMLOctGeometry->NextSiblingElement("octGeometry");
			continue;
		}

		if( XMLOctGeometry->Attribute("vertTotal") )
			VertTotal = StringConverter::parseInt(String(XMLOctGeometry->Attribute("vertTotal")));
		else
		{
			LogManager::getSingleton().logMessage(LML_CRITICAL, "ERROR: DotScene plugin DotSceneOctree::load found no 'vertTotal' xml info *****");
			XMLOctGeometry = XMLOctGeometry->NextSiblingElement("octGeometry");
			continue;
		}

		if( XMLOctGeometry->Attribute("normalTotal") )
			NormalTotal = StringConverter::parseInt(String(XMLOctGeometry->Attribute("normalTotal")));

		if( XMLOctGeometry->Attribute("colorTotal") )
			ColorTotal = StringConverter::parseInt(String(XMLOctGeometry->Attribute("colorTotal")));

		if( XMLOctGeometry->Attribute("tangentTotal") )
			TangentTotal = StringConverter::parseInt(String(XMLOctGeometry->Attribute("tangentTotal")));

		if( XMLOctGeometry->Attribute("texSets") )
			TexSets = StringConverter::parseInt(String(XMLOctGeometry->Attribute("texSets")));

		// Make sure there is something to add
		if( VertTotal )
		{
			// Now Populate the HardwareVertexBuffer
			VertexData* NewVertData = new VertexData;
			NewVertData->vertexStart = 0;
			NewVertData->vertexCount = VertTotal;

			mStream->seek( BinaryDataOffset );

			// Grab the Declaration and Binding pointers		
			VertexDeclaration* decl = NewVertData->vertexDeclaration;			
			VertexBufferBinding* bind = NewVertData->vertexBufferBinding;

			//Used for ?? and keeping a vertices copy for physics
			SharedGeometryData* NewGeomData = new SharedGeometryData();
			NewGeomData->VertData = NewVertData;
			NewGeomData->MaterialName = MaterialName;
			NewGeomData->PhysVertData = new float[ VertTotal * 3 ];
			SharedVertData.push_back( NewGeomData );

			// Read the elements from the .bin file and add to the Vertex Buffer
			readGeometryPositions( decl, bind, VertTotal, NewGeomData->PhysVertData );
			readGeometryNormals( decl, bind, NormalTotal );
			readGeometryColors( decl, bind, ColorTotal );
			readGeometryTangents( decl, bind, TangentTotal );
			readGeometryTexSets( decl, bind, TexSets, VertTotal );
		}

		// Move to the next Geometry Definition
		XMLOctGeometry = XMLOctGeometry->NextSiblingElement("octGeometry");
	}
}

void DotSceneOctree::readGeometryPositions( VertexDeclaration* decl, VertexBufferBinding* bind, int VertTotal,
																					 float *physicsData )
{
	decl->addElement(POSITION_BINDING, 0, VET_FLOAT3, VES_POSITION);
	HardwareVertexBufferSharedPtr PosBufPtr = HardwareBufferManager::getSingleton().createVertexBuffer(
		sizeof(float) * 3, VertTotal, HardwareBuffer::HBU_STATIC_WRITE_ONLY,
		false);
	bind->setBinding( POSITION_BINDING, PosBufPtr );

	float *pPos = static_cast<float*>(PosBufPtr->lock( HardwareBuffer::HBL_DISCARD ));
	mStream->read( pPos, sizeof(float) * VertTotal * 3 );
	//save physics copy
	if( physicsData )
		memcpy( physicsData, pPos, VertTotal * 3 * sizeof(float) );
	PosBufPtr->unlock();
}

void DotSceneOctree::readGeometryNormals( VertexDeclaration* decl, VertexBufferBinding* bind, int NormalTotal )
{
	if( NormalTotal )
	{
		decl->addElement( NORMAL_BINDING, 0, VET_FLOAT3, VES_NORMAL);
		HardwareVertexBufferSharedPtr NormBufPtr = HardwareBufferManager::getSingleton().createVertexBuffer(
			sizeof(float) * 3, NormalTotal, HardwareBuffer::HBU_STATIC_WRITE_ONLY,
			false);
		bind->setBinding( NORMAL_BINDING, NormBufPtr );

		float *pNorm = static_cast<float*>(NormBufPtr->lock( HardwareBuffer::HBL_DISCARD ));
		mStream->read( pNorm, sizeof(float) * NormalTotal * 3 );
		NormBufPtr->unlock();
	}
}

void DotSceneOctree::readGeometryTangents( VertexDeclaration* decl, VertexBufferBinding* bind, int TangentTotal )
{
	if( TangentTotal )
	{
		decl->addElement( TANGENT_BINDING, 0, VET_FLOAT3, VES_TANGENT);
		HardwareVertexBufferSharedPtr TanBufPtr = HardwareBufferManager::getSingleton().createVertexBuffer(
			sizeof(float) * 3, TangentTotal, HardwareBuffer::HBU_STATIC_WRITE_ONLY,
			false);
		bind->setBinding( TANGENT_BINDING, TanBufPtr );

		float *pTan = static_cast<float*>(TanBufPtr->lock( HardwareBuffer::HBL_DISCARD ));
		mStream->read( pTan, sizeof(float) * TangentTotal * 3 );
		TanBufPtr->unlock();
	}
}

void DotSceneOctree::readGeometryColors( VertexDeclaration* decl, VertexBufferBinding* bind, int ColorTotal )
{
	if( ColorTotal )
	{
		decl->addElement( COLOUR_BINDING, 0, VET_FLOAT3, VES_DIFFUSE);
		HardwareVertexBufferSharedPtr ColBufPtr = HardwareBufferManager::getSingleton().createVertexBuffer(
			sizeof(float) * 3, ColorTotal, HardwareBuffer::HBU_STATIC_WRITE_ONLY,
			false);
		bind->setBinding( COLOUR_BINDING, ColBufPtr );

		float* pCol = static_cast<float*>(ColBufPtr->lock( HardwareBuffer::HBL_DISCARD ));
		mStream->read( pCol, sizeof(float) * ColorTotal * 3 );
		ColBufPtr->unlock();
	}
}

void DotSceneOctree::readGeometryTexSets( VertexDeclaration* decl, VertexBufferBinding* bind, int TexSets, int VertTotal )
{
	if( TexSets )
	{
		int TexSetOffset=0;
		for( int CurTexSet=0; CurTexSet<TexSets; CurTexSet++ )
		{
			decl->addElement(TEXCOORD_BINDING, TexSetOffset, VET_FLOAT2, VES_TEXTURE_COORDINATES, CurTexSet);
			TexSetOffset += sizeof( float ) * 2;
		}
		HardwareVertexBufferSharedPtr TexBufPtr = HardwareBufferManager::getSingleton().createVertexBuffer(
			sizeof(float) * 2 * TexSets, VertTotal, HardwareBuffer::HBU_STATIC_WRITE_ONLY,
			false);
		bind->setBinding( TEXCOORD_BINDING, TexBufPtr );

		float* pTex = static_cast<float*>(TexBufPtr->lock( HardwareBuffer::HBL_DISCARD ));
		mStream->read( pTex, sizeof(float) * VertTotal * 2 * TexSets );
		TexBufPtr->unlock();
	}
}

void DotSceneOctree::readNode( SceneNode* ParentNode, TiXmlElement *CurElement )
{
	SceneNode *CurSceneNode;
	CurSceneNode = ParentNode->createChildSceneNode();
	SceneNodeArray.push_back( CurSceneNode );

	TiXmlElement *CurOctreeNode;
	CurOctreeNode = CurElement->FirstChildElement("octNode");

	if( CurOctreeNode )
	{
		while( CurOctreeNode )
		{
			readNode( CurSceneNode, CurOctreeNode );
			CurOctreeNode = CurOctreeNode->NextSiblingElement("octNode");
		}
	}
	else
	{
		// Read the Mesh Elements of this End Node
		TiXmlElement *MeshElement   = CurElement->FirstChildElement("octMesh");
		while( MeshElement )
		{
			TiXmlElement *GeomElement	= MeshElement->FirstChildElement("octGeometry");
			TiXmlElement *MatElement	= MeshElement->FirstChildElement("octMaterial");

			// This will store our geometry
			DotSceneOctreeRenderable *NewRenderable;

			// First load the material. The material name determines what shared vertex buffer
			// this mesh should use.
			// NOTE: A MATERIAL IS A REQUIREMENT. YOU CANT HAVE GEOMETRY WITHOUT A MATERIAL!
			MaterialPtr NewMaterial;
			if( MatElement )
			{
				// Create a new empty Renderable
				NewRenderable = new DotSceneOctreeRenderable();

				//<!ELEMENT oct_material EMPTY>
				//<!ATTLIST oct_material
				//	name CDATA #REQUIRED>
				String MatNameStr;
				if (MatElement->Attribute( "name" ))
					MatNameStr = MatElement->Attribute( "name" );

				NewMaterial = MaterialManager::getSingleton().getByName( MatNameStr );
				if( NewMaterial.isNull() )
				{
					String log = "DotSceneOctree::readNode Material <";
					log += MatNameStr;
					log += "> Not found for renderable, using default ****";
					LogManager::getSingleton().logMessage(log);
					NewMaterial = MaterialManager::getSingleton().getDefaultSettings();
				}

				// Assign the material we created to the renderable
				NewRenderable->setMaterial( NewMaterial );

				// Now Determine the Shared Vertex Buffer that this mesh should use
				std::vector<SharedGeometryData*>::iterator CurSharedData;

				// Search the Shared geometry array to find the one with the same material name
				for( CurSharedData=SharedVertData.begin(); CurSharedData!=SharedVertData.end(); ++CurSharedData )
				{
					if( (*CurSharedData)->MaterialName == MatNameStr ){
						NewRenderable->setSharedGeomPtr( (*CurSharedData)->VertData );
						//Added for coldet - false means that the renderable does not delete the buffer
						NewRenderable->SetVertexPtr( (*CurSharedData)->PhysVertData, false ); 
						break;
					}
				}				

				// load the Index Geometry Next
				if( GeomElement )
				{					
					/*<!ELEMENT oct_geometry EMPTY>
					<!ATTLIST oct_geometry
					binaryDataOffset CDATA #REQUIRED
					triTotal CDATA #REQUIRED>*/
					int binarydataoffset=0, tritotal=0;

					binarydataoffset = StringConverter::parseInt(String(GeomElement->Attribute("binaryDataOffset")));
					tritotal = StringConverter::parseInt(String(GeomElement->Attribute("triTotal")));

					mStream->seek( binarydataoffset );

					// tritotal
					unsigned short *TriData = new unsigned short[ tritotal * 3 ];
					mStream->read( TriData, sizeof(unsigned short) * tritotal * 3 );
					NewRenderable->addIndices( tritotal*3, TriData );
					delete[] TriData;
				}
			} //end if(matElement)

			// Give this new Renderable a Name
			stringstream ss; 
			ss << "OctreeMesh:" << TotalOctreeMeshes;
			NewRenderable->setName( ss.str() );

			TotalOctreeMeshes++;

			// Attach this new geometry to the current node
			CurSceneNode->attachObject( NewRenderable );
			//CurSceneNode->showBoundingBox( true );
			CurSceneNode->_update( true, false );
			RenderableArray.push_back( NewRenderable );

			// Continue looking for more Meshes in this Octree Node
			MeshElement = MeshElement->NextSiblingElement("octMesh");
		}
	}
}

void DotSceneOctree::unload()
{
	// First Detach all Geometry from the Nodes
	//vector <SceneNode*>::iterator CurNode;
	//for( CurNode = SceneNodeArray.begin(); CurNode!=SceneNodeArray.end(); ++CurNode )
	//{
	//	(*CurNode)->detachAllObjects();
	//}
	//SceneNodeArray.clear();
	RootNode->removeAndDestroyAllChildren();

	// Now Destroy all of the geometry
	vector <DotSceneOctreeRenderable*>::iterator CurRenderable;

	for( CurRenderable = RenderableArray.begin(); CurRenderable!=RenderableArray.end(); ++CurRenderable )
	{
		(*CurRenderable)->deleteGeometry();
		delete (*CurRenderable);
	}
	RenderableArray.clear();

	std::vector<SharedGeometryData*>::iterator CurGeom;

	for( CurGeom=SharedVertData.begin(); CurGeom!=SharedVertData.end(); ++CurGeom )
	{
		delete (*CurGeom)->VertData;
		delete[] (*CurGeom)->PhysVertData;
		delete (*CurGeom);
	}
	SharedVertData.clear();
}

float* DotSceneOctree::getVerticesPtr( int MeshIndex )
{
	float* Result = 0;

	if( MeshIndex < TotalOctreeMeshes ){
		Result = RenderableArray[ MeshIndex ]->getVertexPtr();
	}

	return Result;
}

int* DotSceneOctree::getIndexPtr( int MeshIndex )
{
	int* Result = 0;

	if( MeshIndex < TotalOctreeMeshes ){
		Result = RenderableArray[ MeshIndex ]->getIndexPtr();
	}

	return Result;
}

int DotSceneOctree::getNumVertices( int MeshIndex )
{
	int Result = 0;

	if( MeshIndex < TotalOctreeMeshes ){
		Result = RenderableArray[ MeshIndex ]->getNumVertex();
	}

	return Result;
}

int DotSceneOctree::getNumIndex( int MeshIndex )
{
	int Result = 0;

	if( MeshIndex < TotalOctreeMeshes ){
		Result = RenderableArray[ MeshIndex ]->getNumIndex();
	}

	return Result;
}

std::vector <DotSceneOctreeRenderable*> DotSceneOctree::getAllRenderables()
{
	return RenderableArray;
}
