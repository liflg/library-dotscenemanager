////////////////////////////////////////////////////////////////////////////////
// DotSceneOctreeRenderable.h
// Author     : Doug Wolanick
// Start Date : May 27, 2003
// Copyright  : (C) 2003 by Doug Wolanick
// Email      : Doug@IceTecStudios.com
////////////////////////////////////////////////////////////////////////////////

#include "SceneOctreeRenderable.h"
#include "OgreSceneNode.h"
#include "OgreRenderQueue.h"
#include "OgreRenderOperation.h"
#include "OgreCamera.h"
#include "OgreRoot.h"

int DotSceneOctreeRenderable::mRenderedTris = 0;

DotSceneOctreeRenderable::DotSceneOctreeRenderable()
{
	MaterialIndex = 0;

	numOldVerts = 0;
	numIndices = 0;

	mInit = false;

	mColored			 = false;
	mLit			     = false;

	mVertData = 0;
	indexData = 0;
	PhysVertData = 0;
	PhysIndexData = 0;

	autoDeleteVD = false;
	PhysVertData = 0;
}

DotSceneOctreeRenderable::~DotSceneOctreeRenderable()
{
}

void DotSceneOctreeRenderable::deleteGeometry()
{
	//delete mVertData;
	delete indexData;

	if( autoDeleteVD )
		delete[] PhysVertData;

	delete[] PhysIndexData;

	mVertData = 0;
	indexData = 0;
	PhysVertData = 0;
	PhysIndexData = 0;
}

void DotSceneOctreeRenderable::setSharedGeomPtr( VertexData *NewSharedPtr )
{
	mVertData = NewSharedPtr;
}

void DotSceneOctreeRenderable::addVertices( int VertTotal , float *VertData )
{
	/*
	mVertData = new VertexData;
	mVertData->vertexStart = 0;
	mVertData->vertexCount = VertTotal;

	PhysVertData = new Real[ VertTotal*3 ];

	VertexDeclaration* decl = mVertData->vertexDeclaration;
	VertexBufferBinding* bind = mVertData->vertexBufferBinding;

	// positions
	decl->addElement(POSITION_BINDING, 0, VET_FLOAT3, VES_POSITION);

	HardwareVertexBufferSharedPtr vbuf =
	HardwareBufferManager::getSingleton().createVertexBuffer(
	3 * sizeof( Real ),
	mVertData->vertexCount, 
	HardwareBuffer::HBU_STATIC_WRITE_ONLY,
	false);

	bind->setBinding(POSITION_BINDING, vbuf);

	vbuf = bind->getBuffer(POSITION_BINDING);
	Real* pPos = static_cast<Real*>(vbuf->lock(HardwareBuffer::HBL_DISCARD));

	// Bounding Box Vars
	int Min[3], Max[3];
	Min[0]=Min[1]=Min[2] =  1000000 ;
	Max[0]=Max[1]=Max[2] = -1000000 ;

	int CurAxis=0;

	for( int index=0; index<VertTotal*3; index++ )
	{

	*pPos++ = VertData[index];
	PhysVertData[index] = VertData[index];		

	// Update the Bounding Box //
	if( VertData[ index ] < Min[CurAxis] )
	Min[CurAxis] = VertData[ index ];

	if( VertData[ index ] > Max[CurAxis] )
	Max[CurAxis] = VertData[ index ];


	CurAxis = ((CurAxis+1)%3);
	}

	vbuf->unlock();

	// Set the bounding box //
	mBounds.setExtents( Min[0] , Min[1] , Min[2] , Max[0] , Max[1] , Max[2] );
	mCenter = Vector3( (Max[0]+Min[0])/2 , (Max[1]+Min[1])/2 , (Max[2]+Min[2])/2 );

	mInit = true;
	*/
}

// Note: It is assumed that there is a 1:1 relationship between the following:
// Vertices, Normals, UVs, Colors
void DotSceneOctreeRenderable::addNormals( int NormalTotal , float *NormalData )
{
	/*
	VertexDeclaration* decl = mVertData->vertexDeclaration;
	VertexBufferBinding* bind = mVertData->vertexBufferBinding;

	// positions
	decl->addElement(NORMAL_BINDING, 0, VET_FLOAT3, VES_NORMAL);    

	HardwareVertexBufferSharedPtr vbuf =
	HardwareBufferManager::getSingleton().createVertexBuffer(
	3 * sizeof( Real ),
	mVertData->vertexCount, 
	HardwareBuffer::HBU_STATIC_WRITE_ONLY,
	false);

	bind->setBinding(NORMAL_BINDING, vbuf);

	vbuf = bind->getBuffer(NORMAL_BINDING);
	Real* pPos = static_cast<Real*>(vbuf->lock(HardwareBuffer::HBL_DISCARD));

	for( unsigned int index=0 ; index<mVertData->vertexCount*3; index++ )
	{
	*pPos++ = NormalData[ index ];
	}

	vbuf->unlock();
	*/
}

void DotSceneOctreeRenderable::addColors( int ColorTotal , float *ColorData )
{


}

void DotSceneOctreeRenderable::addTexCoords( int TexCoordTotal , float *TexCoordData , int TexSet,  int TotalTexSets )
{
	/*
	HardwareVertexBufferSharedPtr vbuf;

	VertexDeclaration* decl = mVertData->vertexDeclaration;
	VertexBufferBinding* bind = mVertData->vertexBufferBinding;

	if( TexSet == 0 ){
	HardwareVertexBufferSharedPtr vbuf =
	HardwareBufferManager::getSingleton().createVertexBuffer(
	VertexElement::getTypeSize(VET_FLOAT2) * TotalTexSets,
	mVertData->vertexCount, 
	HardwareBuffer::HBU_STATIC_WRITE_ONLY,
	false);

	bind->setBinding(TEXCOORD_BINDING, vbuf);
	}


	// positions    
	decl->addElement(TEXCOORD_BINDING, TexSet * VertexElement::getTypeSize(VET_FLOAT2),
	VET_FLOAT2, VES_TEXTURE_COORDINATES, TexSet);

	vbuf = bind->getBuffer(TEXCOORD_BINDING);
	Real* pPos = static_cast<Real*>(vbuf->lock(HardwareBuffer::HBL_DISCARD));

	unsigned int index, index2;
	for( index=0, index2=(TexSet*2) ; index<mVertData->vertexCount*2; index+=2, index2+=(2*TotalTexSets) )
	{
	pPos[index2  ] = TexCoordData[ index   ];
	pPos[index2+1] = TexCoordData[ index+1 ];
	}

	vbuf->unlock();
	*/

}

void DotSceneOctreeRenderable::addIndices( int IndexTotal , unsigned short *IndexArray )
{

	indexData = new IndexData;
	indexData->indexCount=IndexTotal;
	indexData->indexStart=0;

	PhysIndexData = new int[IndexTotal];

	indexData->indexBuffer = 
		HardwareBufferManager::getSingleton().createIndexBuffer(
		HardwareIndexBuffer::IT_16BIT,
		IndexTotal, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false );

	unsigned short* pIdx = static_cast<unsigned short*>(
		indexData->indexBuffer->lock(0, 
		indexData->indexBuffer->getSizeInBytes(), 
		HardwareBuffer::HBL_DISCARD));

	for( int index=0; index<IndexTotal; index++ )
	{
		*pIdx++ = IndexArray[index];
		PhysIndexData[index] = (int)IndexArray[index];
	}

	indexData->indexBuffer->unlock();

	// Setup the New Data //
	numIndices = IndexTotal;

	// Now determine the bounding box
	setupBoundingBox( IndexArray );

}

void DotSceneOctreeRenderable::setupBoundingBox( unsigned short *IndexArray )
{
	// Bounding Box Vars
	int Min[3], Max[3];
	Min[0]=Min[1]=Min[2] =  1000000 ;
	Max[0]=Max[1]=Max[2] = -1000000 ;

	// Grab a pointer to the vertex shadow buffer
	VertexDeclaration* decl = mVertData->vertexDeclaration;
	VertexBufferBinding* bind = mVertData->vertexBufferBinding;

	HardwareVertexBufferSharedPtr vbuf;
	vbuf = bind->getBuffer(POSITION_BINDING);
	//XXX: Buffers are HBU_STATIC_WRITE_ONLY, should they be?
	Real* pPos = static_cast<Real*>(vbuf->lock(HardwareBuffer::HBL_NORMAL/*HBL_READ_ONLY*/));

	for( int index=0; index<numIndices; index++ )
	{
		for( int CurAxis=0; CurAxis<3; CurAxis++ )
		{
			// Update the Bounding Box //
			if( pPos[ (IndexArray[ index ]*3)+CurAxis ] < Min[CurAxis] )
				Min[CurAxis] = (int) pPos[ (IndexArray[ index ]*3)+CurAxis ];

			if( pPos[ (IndexArray[ index ]*3)+CurAxis ] > Max[CurAxis] )
				Max[CurAxis] = (int) pPos[ (IndexArray[ index ]*3)+CurAxis ];
		}
	}

	vbuf->unlock();

	// Set the bounding box //
	mBounds.setExtents( Min[0] , Min[1] , Min[2] , Max[0] , Max[1] , Max[2] );
	mCenter = Vector3( (Max[0]+Min[0])/2 , (Max[1]+Min[1])/2 , (Max[2]+Min[2])/2 );

	Real tempX,tempY,tempZ;
	// max (xMax,|xMin|)
	if (abs(Min[0]) > abs(Max[0]))
		tempX = abs(Min[0]);
	else
		tempX = abs(Max[0]);

	// max (yMax,|yMin|)
	if (abs(Min[1]) > abs(Max[1]))
		tempY = abs(Min[1]);
	else
		tempY = abs(Max[1]);
	// max (zMax,|zMin|)
	if (abs(Min[2]) > abs(Max[2]))
		tempZ = abs(Min[2]);
	else
		tempZ = abs(Max[2]);
	// sqrt[ max(xMax,|xMin|)^2 + max(yMax,|yMin|)^2 + max(zMax,|zMin|)^2 ]
	mBoundingRadius = sqrt(tempX*tempX + tempY*tempY + tempZ*tempZ);
	mInit = true;
}

void DotSceneOctreeRenderable::setMaterialIndex( int NewIndex )
{
	MaterialIndex = NewIndex;
}

int DotSceneOctreeRenderable::getMaterialIndex()
{
	return MaterialIndex;
}

void DotSceneOctreeRenderable::init( )
{
	deleteGeometry();

	//calculate min and max heights;
	Real min = 256000, max = 0;

	int m = 0;

	int q = 0;

}



void DotSceneOctreeRenderable::_updateRenderQueue( RenderQueue* queue )
{
	queue->addRenderable(this, mRenderQueueID, OGRE_RENDERABLE_DEFAULT_PRIORITY);
}


void DotSceneOctreeRenderable::_notifyCurrentCamera( Camera* cam )
{
	int index=0;
}

Real DotSceneOctreeRenderable::getSquaredViewDepth(const Camera* cam) const
{
	Vector3 diff = mCenter - cam->getDerivedPosition();
	// Use squared length to avoid square root
	return diff.squaredLength();
}

void DotSceneOctreeRenderable::getRenderOperation( RenderOperation& rend )
{

	rend.useIndexes = true;
	rend.operationType = RenderOperation::OT_TRIANGLE_LIST;
	rend.vertexData = mVertData;
	rend.indexData = indexData;

	mRenderedTris += ( numIndices / 3 );
}

void DotSceneOctreeRenderable::getWorldTransforms( Matrix4* xform ) const
{
	*xform = mParentNode->_getFullTransform();
}

const Quaternion& DotSceneOctreeRenderable::getWorldOrientation(void) const
{
	return mParentNode->_getDerivedOrientation();
}
const Vector3& DotSceneOctreeRenderable::getWorldPosition(void) const
{
	return mParentNode->_getDerivedPosition();
}

const LightList& DotSceneOctreeRenderable::getLights(void) const
{
	if ((SceneNode*)mParentNode)
	{
		const_cast<DotSceneOctreeRenderable*>(this)->mLightList.clear();
		const_cast<LightList&>(mLightList) = ((SceneNode*)mParentNode)->findLights(this->getBoundingRadius());
		return mLightList;
	}
	else
		return mParentNode->getLights();
}
