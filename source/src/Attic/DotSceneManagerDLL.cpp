#include "OgreRoot.h"
#include "DotSceneManager.h"

namespace Ogre
{
	DotSceneOctreeManagerFactory* DotScenePlugin;

	extern "C" void _PluginDotSceneManagerExport dllStartPlugin( void )
	{
		// Create new scene manager
		DotScenePlugin = new DotSceneOctreeManagerFactory();

		// Register
		Root::getSingleton().addSceneManagerFactory(DotScenePlugin);
	}

	extern "C" void _PluginDotSceneManagerExport dllShutdownPlugin( void )
	{
		Root::getSingleton().removeSceneManagerFactory(DotScenePlugin);
	}

	extern "C" void _PluginDotSceneManagerExport dllStopPlugin( void )
	{
		delete DotScenePlugin;
	}
}
