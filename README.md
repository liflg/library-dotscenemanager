Website
=======
http://www.ogre3d.org/

License
=======
LGPL v2 (see the file source/include/Attic/DotSceneManager.h)

Version
=======
Subversion tag v0-2-0

Source
======
http://svn.code.sf.net/p/ogreaddons/code/tags/v0-2-0/dotsceneoctree/PlugIns/DotSceneManager

Requires
========
* ogre12