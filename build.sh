#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    read SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        CXXFLAGS="$CXXFLAGS -ggdb"
    else
        CXXFLAGS="$CXXFLAGS"
    fi

    if [ -z "$OPTIMIZATION" ]; then
        CXXFLAGS="$CXXFLAGS -O2"
    else
        CXXFLAGS="$CXXFLAGS -O$OPTIMIZATION"
    fi

    mkdir "$BUILDDIR"
    rm -rf "$PREFIXDIR"

    ( OGRE12_DIR="${PWD}/../library-ogre12/${MULTIARCHNAME}"
      export CXXFLAGS="$CXXFLAGS -I${OGRE12_DIR}/include/OGRE"
      cd "$BUILDDIR"
      cmake -DCMAKE_INSTALL_PREFIX:PATH="$PREFIXDIR" ../source
      make -j $(nproc)
      make install)
}

if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="$PWD/build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/include/Attic/DotSceneManager.h "$PREFIXDIR"/lib/LICENSE-dotscenemanager.txt

rm -rf "$BUILDDIR"

echo "DotSceneManager for $MULTIARCHNAME is ready."
