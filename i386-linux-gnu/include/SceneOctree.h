#ifndef __SCENE_OCTREE_H_
#define __SCENE_OCTREE_H_

#include "Ogre.h"
#include "OgreStringConverter.h"
#include "SceneOctreeRenderable.h"
#include "OgreDataStream.h"
#include "tinyxml.h"
#include <vector>

using namespace Ogre;

struct SharedGeometryData
{
	VertexData* VertData;
	float* PhysVertData;
	String MaterialName;
};

class DotSceneOctree
{
public:
	DotSceneOctree();
	~DotSceneOctree();

	// load the <Octree> Data
	int load( TiXmlElement *Element, const String& groupname );
	void unload();

	// Must be called before load is called //
	void setSceneManager( SceneManager *NewSceneManager ){
		mSceneManager = NewSceneManager;
	}

	void setRootNode( SceneNode* ParentNode ){
		RootNode = ParentNode->createChildSceneNode();
	}

	int getTotalMeshes( void ){
		return TotalOctreeMeshes;
	}

	void setOctreeVisible( bool Visible );

	float* getVerticesPtr( int MeshIndex );
	int*  getIndexPtr( int MeshIndex );
	int getNumVertices( int MeshIndex );
	int getNumIndex( int MeshIndex );

	const AxisAlignedBox& getBoundingBox( int MeshIndex )
	{
		return RenderableArray[ MeshIndex ]->getBoundingBox();
	}

	std::vector <DotSceneOctreeRenderable*> getAllRenderables( void );

private:

	bool OctreeVisible;

	enum BranchID{
		TopFrontLeft,
		TopFrontRight,
		TopRearLeft,
		TopRearRight,
		BottomFrontLeft,
		BottomFrontRight,
		BottomRearLeft,
		BottomRearRight
	};

	// load Functions //
	void readNode( Ogre::SceneNode *ParentNode, TiXmlElement* CurElement );
	void loadSharedGeometry( TiXmlElement* XMLSharedGeometry );

	void readGeometryTexSets( VertexDeclaration* decl, VertexBufferBinding* bind, int TexSets, int VertTotal );
	void readGeometryColors( VertexDeclaration* decl, VertexBufferBinding* bind, int ColorTotal );
	void readGeometryNormals( VertexDeclaration* decl, VertexBufferBinding* bind, int NormalTotal );
	void readGeometryTangents( VertexDeclaration* decl, VertexBufferBinding* bind, int TangentTotal );
	void readGeometryPositions( VertexDeclaration* decl, VertexBufferBinding* bind, int VertTotal, float *physicsData );

	DataStreamPtr mStream;
	int TotalOctreeMeshes;

	SceneNode	 *RootNode;
	SceneManager *mSceneManager;

	std::vector <DotSceneOctreeRenderable*> RenderableArray;
	std::vector <SceneNode*> SceneNodeArray;

	// All vertex information is shared from here. Each Mesh in the octree has a pointer
	// to the correct VertexData pointer in this array.
	std::vector<SharedGeometryData*> SharedVertData;

	// Unused for now
	//String Name;
};
#endif
