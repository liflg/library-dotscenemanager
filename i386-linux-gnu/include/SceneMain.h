#ifndef __SCENE_MAIN_H_
#define __SCENE_MAIN_H_

#include "Ogre.h"
#include "SceneOctree.h"
//#include "SceneTerrain.h"
#include "OgreStringConverter.h"
#include "tinyxml.h"

struct FogInfo{
	Ogre::FogMode Type;
	Ogre::ColourValue Color;
	float Density, Start, End;
};

class DotSceneMain
{
public:
	DotSceneMain();
	~DotSceneMain();

	void setSceneManager( Ogre::SceneManager* NewSceneManager );

	// load and save the .scene File
	int  load( const Ogre::String& Filename, const Ogre::String& groupname );
	void unload();

	void setName( Ogre::String NewName );
	Ogre::String getName( void );

	void setPos( Ogre::Vector3 NewPos );
	void setPos( float x, float y, float z );

	// Set the Terrain in the Scene
	void setTerrainGeometry( Ogre::String Filename );
	void setTerrainTexture( Ogre::String MaterialName, Ogre::String Filename=0 );

	// Return the Geometry Data For Collision //
	int   getNumMeshes( char* name );
	float* getVerticesPtr( char* name, int MeshIndex );
	int*  getIndexPtr( char* name, int MeshIndex  );
	int getNumVertices( char* name, int MeshIndex );
	int getNumIndex( char* name, int MeshIndex  );

	const AxisAlignedBox& getBoundingBox( char* name, int MeshIndex );

	void setOctreeVisible( bool Visible );

	std::vector <DotSceneOctreeRenderable*> getAllRenderables( void );

private:

	bool OctreeVisible;

	Ogre::String MyName;

	// Environment Variables
	FogInfo Fog;
	Ogre::ColourValue Ambient;
	// Loads the Environment Settings
	void loadEnvironment( TiXmlElement *Element );

	// Location of this Scene Object
	Ogre::Vector3 Position;

	DotSceneOctree	 *Octree;
	//SceneTerrain *Terrain;

	Ogre::SceneNode* RootNode;

	Ogre::SceneManager* mSceneManager;
};

#endif
