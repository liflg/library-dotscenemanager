////////////////////////////////////////////////////////////////////////////////
// DotSceneOctreeRenderable.h
// Author     : Doug Wolanick
// Start Date : July 24, 2003
// Copyright  : (C) 2003 by Doug Wolanick
// Email      : Doug@IceTecStudios.com
////////////////////////////////////////////////////////////////////////////////

#ifndef _SCENE_OCTREE_RENDERABLE_H
#define _SCENE_OCTREE_RENDERABLE_H

#include "OgreRenderable.h"
#include "OgreMovableObject.h"
#include "OgreAxisAlignedBox.h"
#include "OgreString.h"
#include "OgreHardwareBufferManager.h"

#include <vector>

#define POSITION_BINDING 0
#define NORMAL_BINDING 1
#define TEXCOORD_BINDING 2
#define COLOUR_BINDING 3
#define TANGENT_BINDING 4

using namespace Ogre;

class DotSceneOctreeRenderable : public Renderable, public MovableObject
{
public:

	DotSceneOctreeRenderable();
	~DotSceneOctreeRenderable();

	void deleteGeometry();
	void init();


	void setSharedGeomPtr( VertexData* NewSharedPtr );
	void addVertices( int VertTotal, float* VertData );
	void addNormals( int NormalTotal, float* NormalData );
	void addColors( int ColorTotal, float* ColorData );
	void addTexCoords( int TexCoordTotal, float* TexCoordData , int TexSet=0, int TotalTexSets=1);
	void addIndices( int IndexTotal , unsigned short *IndexArray );
	void setMaterialIndex( int NewIndex );

	int getMaterialIndex();

	//movable object methods
	const String& getName( void ) const {return mName;};
	void setName( const String &NewName ) {mName = NewName;}

	virtual const String& getMovableType( void ) const
	{
		return mType;
	};

	const AxisAlignedBox& getBoundingBox( void ) const
	{
		return mBounds;
	};

	// Perhaps not used
	virtual void _notifyCurrentCamera( Camera* cam );

	virtual void _updateRenderQueue( RenderQueue* queue );

	//virtual void getLegacyRenderOperation( LegacyRenderOperation& rend );

	// Only 1 material per renderable
	virtual const MaterialPtr& getMaterial( void ) const
	{
		return mMaterial;
	};

	virtual void getWorldTransforms( Matrix4* xform ) const;

	virtual const Quaternion& getWorldOrientation(void) const;
	virtual const Vector3& getWorldPosition(void) const;

	/** @copydoc Renderable::getLights */
	const LightList& getLights(void) const;


	void setMaterial( MaterialPtr m )
	{
		mMaterial = m;
	};

	virtual void getRenderOperation( RenderOperation& rend );

	// Overridden //
	Real getSquaredViewDepth(const Camera* cam) const;

	/** Overridden from MovableObject */
	Real getBoundingRadius(void) const { return 0; /* not needed */ }


	static int mRenderedTris;

	float* getVertexPtr( void ){
		return PhysVertData;
	}

	void SetVertexPtr( float* pVertexPtr, bool autoDelete = false ) {
		autoDeleteVD = autoDelete;
		PhysVertData = pVertexPtr;
	}


	int* getIndexPtr( void ){
		return PhysIndexData;
	}

	size_t getNumVertex( void ){
		return mVertData->vertexCount;
	}

	size_t getNumIndex( void ){
		return indexData->indexCount;
	}


protected:

	void setupBoundingBox( unsigned short *IndexArray );

	Real mBoundingRadius; 

	AxisAlignedBox mBounds;
	Vector3 mCenter;
	Vector3 mScale;

	// needed for findLights
	LightList mLightList;

	int mSize;
	int mWorldSize;

	String mName;
	String mType;

	int MaterialIndex;
	MaterialPtr mMaterial;

	bool mInit;

	//GeometryData mGeomData;
	VertexData* mVertData;
	IndexData* indexData;

	// Physics Geometry Data
	float* PhysVertData;
	bool autoDeleteVD;
	int*  PhysIndexData;

	int mNearPlane;
	int mMaxPixelError;
	int mVertResolution;
	Real mTopCoord;

	bool mColored;
	bool mLit;

	int numOldVerts;
	int numIndices;
	//unsigned short *pIndices; 
};
#endif
